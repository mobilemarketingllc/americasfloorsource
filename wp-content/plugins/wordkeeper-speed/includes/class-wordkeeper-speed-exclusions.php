<?php

/**
 * This class handles the exclusions process with the conflicting plugins
 *
 * @package:     WordKeeper Speed Exclusions
 * @author:  WordKeeper
 * @Author URI: http://www.wordkeeper.com
 *
 */
class WordKeeper_Speed_Exclusions {

	// An instance of active WordPress plugins
	public $wordkeeper_active_plugins = array();

	/**
	 * Settings of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      array    $settings    Settings of this plugin.
	 */
	private $settings;


	/**
	 * __construct function.
	 *
	 * @access public
	 * @param mixed $settings
	 * @return void
	 */
	public function __construct($settings) {
		$this->settings = $settings;
	}

	/**
	 * Initialize the class and register the filter for exclusion
	 *
	 * @access public
	 * @param array $wordkeeper_active_plugins
	 * @return void
	 */
	public function init($wordkeeper_active_plugins) {
		$this->wordkeeper_active_plugins = $wordkeeper_active_plugins;
		add_filter('wordkeeper_filter_overrides', array($this, 'process_exclusions'), 10, 2);
	}

	/**
	 * Performs the exclusion process to override conflicting settings
	 *
	 * @access public
	 * @param mixed $function
	 * @param mixed $buffer
	 * @return bool
	 */
	public function process_exclusions($function, $buffer) {
		//global $wordkeeper, $wordkeeper_map, $wordkeeper_filtered;

		// Map function to setting for review
		$setting = $this->settings['wordkeeper_map'][$function];

		$conflicts = $this->get_conflicts();
		$rules = $this->get_override_rules($buffer);

		foreach($conflicts as $conflict){
			/*
			*	Skip the following type of plugins
			*	1) that have no known conflict with WordKeeper
			*	2) the plugins that have a known conflict with WordKeeper are not installed.
			*/

			if(!in_array($conflict, $this->wordkeeper_active_plugins)){
				continue;
			}

			// If already filtered, then return the filtered setting
			if(isset($this->settings['wordkeeper_filtered'][$setting]) && $this->settings['wordkeeper_filtered'][$setting] === true) {
				return $rules[$conflict]['overrides'][$setting];
			}

			// Process unique conditions and also add to the filtered array
			if($rules[$conflict]['unique_conditions'] && isset($rules[$conflict]['overrides'][$setting])) {
				Wordkeeper_Speed_Config::get_instance()->update_filtered($setting, true);
				$this->settings = Wordkeeper_Speed_Config::get_instance()->get_config();
				//$wordkeeper_filtered[$setting] = true;

				return $rules[$conflict]['overrides'][$setting];
			}
		}

		return $this->settings['wordkeeper'][$setting];
	}

	/**
	 * Returns the list of rules for known conflicting plugins
	 *
	 * @access private
	 * @param mixed $buffer
	 * @return array
	 */
	private function get_override_rules($buffer) {
		$rules = array(
			'bb-plugin/fl-builder.php' => array(
				'overrides' => array('lazyload-images' => false, 'lazyload-iframes' => false, 'lazyload-videos' => false, 'defer-javascript' => false),
				'unique_conditions' => isset($_GET['fl_builder'])
			),
			'leadpages/leadpages.php' => array(
				'overrides' => array('defer-javascript' => false, 'lazyload-images' => false, 'lazyload-videos' => false, 'use-deferscript' => false),
				'unique_conditions' => strpos($buffer, '<meta name="leadpages-served-by" content="wordpress">') !== false
			),
			'css-hero/css-hero-main.php' => array(
				'overrides' => array('defer-javascript' => false),
				'unique_conditions' => isset($_GET['csshero_action'])
			)
		);
		return $rules;
	}

	/**
	 * Returns the list of conflicting plugins
	 *
	 * @access private
	 * @return array
	 */
	private function get_conflicts() {
		// List of plugins that have a known conflict with WordKeeper
		return array(
			'bb-plugin/fl-builder.php',
			'leadpages/leadpages.php',
			'css-hero/css-hero-main.php'
		);
	}
}
