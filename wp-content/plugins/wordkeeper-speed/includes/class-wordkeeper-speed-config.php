<?php

/**
 * The file that defines the core plugin configuration
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://wordkeeper.com/
 * @since      1.0.0
 *
 * @package    Wordkeeper_Speed
 * @subpackage Wordkeeper_Speed/includes
 */

/**
 * The core plugin config class which implements Singleton Design Pattern.
 *
 * This is used to define configuration variables.
 *
 *
 * @since      1.0.0
 * @package    Wordkeeper_Speed
 * @subpackage Wordkeeper_Speed/includes
 * @author     Lance Dockins <info@wordkeeper.com>
 */
class Wordkeeper_Speed_Config {

	protected static $instance;
	protected $initialized = false;
	protected $wordkeeper = array();
	protected $wordkeeper_overrides = array();
	protected $wordkeeper_filtered = array();
	protected $wordkeeper_map = array();
	protected $wordkeeper_schema_version = 1;

	/**
	 * Initializes this class and related variables
	 *
	 * @access public
	 * @param void
	 * @return void
	 */
	public function init() {
		if($this->initialized)
			return;

		$this->wordkeeper = $this->get_default_config();

		$this->wordkeeper_map = array(
			'process_buffer_images' => 'lazyload-images',
			'process_buffer_videos' => 'lazyload-videos',
			'process_buffer_iframes' => 'lazyload-iframes',
			'lazyload_video' => 'lazyload-videos',
			'lazyload_script_videos' => 'lazyload-videos',
			'lazyload_image' => 'lazyload-images',
			'lazyload_iframe' => 'lazyload-iframes',
			'defer_buffer_scripts' => 'defer-javascript',
			'apply_deferscript' => 'use-deferscript',
			'strip_duplicate_assets' => 'strip-duplicate-assets',
			'strip_session_cookie' => 'strip-session-cookie',
			'strip_comments' => 'strip-comments',
			'remove_google_maps' => 'disable-maps-api',
			'concatenate_google_fonts' => 'optimize-google-fonts',
			'remove_query_strings' => 'remove-query-strings'
		);

		$site_config = $this->get_site_config();

		if($site_config && is_array($site_config)){
			$this->wordkeeper = $site_config;
		}

		require_once plugin_dir_path( __FILE__ ) . 'class-wordkeeper-speed-exclusions.php';
		$wordkeeper_active_plugins = get_option('active_plugins');
		$wordkeeper_exclusions = new WordKeeper_Speed_Exclusions($this->get_config());
		$wordkeeper_exclusions->init($wordkeeper_active_plugins);
		unset($wordkeeper_active_plugins);

		$this->wordkeeper = $this->calculate_dynamic_values($this->wordkeeper);
		$this->initialized = true;
	}


	/**
	 * __construct function.
	 *
	 * @access private
	 * @return void
	 */
	private function __construct() {}

	/**
	 * Either returns an existing single class instance or creates a new and returns it.
	 *
	 * @access static
	 * @param void
	 * @return $instance
	 */
	public static function get_instance() {
		if(is_null(self::$instance)){
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
	 * Returns an array of all config variables
	 *
	 * @access public
	 * @param void
	 * @return array
	 */
	public function get_config() {
		$site_config = $this->get_site_config();
		$config = $site_config ? $site_config : $this->wordkeeper;
		$config = $this->calculate_dynamic_values($config);

		return array(
			'wordkeeper' => $config,
			'wordkeeper_overrides' => $this->wordkeeper_overrides,
			'wordkeeper_filtered' => $this->wordkeeper_filtered,
			'wordkeeper_map' => $this->wordkeeper_map
		);
	}

	/**
	 * Calculates all dynamic variables, adds it to the original array and returns it back.
	 *
	 * @access public
	 * @param array $wordkeeper
	 * @return array
	 */
	public function calculate_dynamic_values($wordkeeper){
		$wordkeeper['use-buffering'] = ($wordkeeper['defer-javascript'] || $wordkeeper['lazyload-images'] || $wordkeeper['lazyload-iframes'] || $wordkeeper['lazyload-videos'] || $wordkeeper['disable-maps-api'] || $wordkeeper['strip-duplicate-assets'] || $wordkeeper['minify-js'] || $wordkeeper['minify-css'] || $wordkeeper['use-deferscript']) ? true : false;
		$wordkeeper['capture-scripts'] = ($wordkeeper['defer-javascript'] || $wordkeeper['disable-maps-api'] || $wordkeeper['deferscript-delay'] || $wordkeeper['minify-js']) ? true : false;
		$wordkeeper['capture-iframes'] = ($wordkeeper['lazyload-iframes'] || $wordkeeper['lazyload-videos']) ? true : false;
		$wordkeeper['capture-images'] = ($wordkeeper['lazyload-images']) ? true : false;
		$wordkeeper['capture-links'] = ($wordkeeper['strip-duplicate-assets'] || $wordkeeper['minify-css']) ? true : false;
		$wordkeeper['minify'] = ($wordkeeper['minify-css'] || $wordkeeper['minify-js']) ? true : false;
		$wordkeeper['partials'] = ($wordkeeper['cache-widgets'] || $wordkeeper['cache-menus'] || $wordkeeper['cache-fragments']) ? true : false;

		$urlparts = parse_url($_SERVER['REQUEST_URI']);
		$urlparts = trim($urlparts['path'], '/');
		$urlparts = explode('/', $urlparts);
		$wordkeeper['amp'] = ($urlparts[count($urlparts) - 1] == 'amp') ? true : false;
		unset($urlparts);
		return $wordkeeper;
	}

	/**
	 * Returns an array of all default configurations
	 *
	 * @access public
	 * @param void
	 * @return array
	 */
	public function get_default_config(){
		return array(
			'defer-javascript' => true,
			'disable-header-garbage' => true,
			'optimize-db-count-queries' => true,
			'remove-query-strings' => true,
			'disable-emojis' => true,
			'disable-wordpress-embeds' => true,
			'minify-js' => false,
			'minify-css' => false,
			'lazyload-images' => true,
			'lazyload-iframes' => false,
			'lazyload-mode' => 'near', // options (near, visible, none)
			'lazyload-videos' => true,
			'lazyload-videos-mode' => 'default',  // options (default:lightbox)
			'disable-maps-api' => false,
			'strip-duplicate-assets' => false,
			'strip-comments' => false,
			'strip-session-cookie' => false,
			'optimize-google-fonts' => true,
			'disable-woocommerce-refresh-fragments' => true,
			'use-deferscript' => false,
			'deferscript-delay' => '1500', // number of milliseconds to delay
			'disable-plugins-update-check' => true,
			'disable-optimization-for-editors' => false,
			'disable-slow-woocommerce-transient-purges' => false,
			'enable-scheduled-transient-purges' => true,
			'async-woocommerce-emails' => false,
			'http-resource-hints' => true,
			'cache-widgets' => true,
			'cache-menus' => true,
			'cache-fragments' => true,
			'cache-folder' => 'product',
			'cache-duration' => 86400   //  24 hours in seconds
		);
	}


	/**
	 * get_config_path function.
	 *
	 * @access public
	 * @return void
	 */
	public function get_config_path(){
		if ( is_multisite() ){
			return ABSPATH . 'wp-content/wordkeeper-speed/sites/' .get_current_blog_id();
		}
		return ABSPATH . 'wp-content/wordkeeper-speed';
	}

	/**
	 * Checks to see if a site specific configs exists and returns it (Array). If there's none then returns default config.
	 *
	 * @access public
	 * @param void
	 * @return mixed
	 */
	public function get_site_config(){
		$file = $this->get_config_path() . '/config.php';
		if(file_exists($file)){
			include $file;
			return $config;
		}
		else{
			$this->create_default_site_config();
			include $file;
			return $config;
		}
	}

	/**
	 * Checks to see if a site specific config file exists
	 *
	 * @access public
	 * @param void
	 * @return bool
	 */
	public function site_config_exists(){
		$file = $this->get_config_path() . '/config.php';
		if(file_exists($file)){
			return true;
		}
		return false;
	}

	/**
	 * Updates the site specific config file with the new settings using the config template.
	 *
	 * @access public
	 * @param array $settings
	 * @return bool
	 */
	public function update_site_config($settings){
		$templatePath = plugin_dir_path( __FILE__ ) . '../wordkeeper-speed-config.tpl';
		$template = "";
		if(file_exists($templatePath)){
			$template = file_get_contents($templatePath);
			foreach($settings as $setting => $v){
				$template = str_replace('['.$setting.']', $v, $template);
			}
			$file = $this->get_config_path() . '/config.php';
			if(!is_dir($this->get_config_path())){
				mkdir($this->get_config_path(), 0775, true);
			}
			file_put_contents($file, $template);
			return true;
		}
		return false;
	}

	/**
	 * Creates a new site specific config file using the default config values.
	 *
	 * @access public
	 * @param void
	 * @return bool
	 */
	public function create_default_site_config(){
		$settings = $this->get_default_config();
		$templatePath = plugin_dir_path( __FILE__ ) . '../wordkeeper-speed-config.tpl';
		$template = "";

		if(file_exists($templatePath)){
			$template = file_get_contents($templatePath);
			foreach($settings as $setting => $v){
				$value = NULL;
				if(is_bool($v)){
					$value = $v ? "true" : "false";
				}
				else{
					$value = $v;
				}
				$template = str_replace('['.$setting.']', $value, $template);
			}
			$template = str_replace('[schema-version]', $this->wordkeeper_schema_version, $template);
			$file = $this->get_config_path() . '/config.php';
			if(!is_dir($this->get_config_path())){
				mkdir($this->get_config_path(), 0775, true);
			}
			file_put_contents($file, $template);
			return true;
		}
	}


	/**
	 * requires_schema_update function.
	 *
	 * @access public
	 * @return void
	 */
	public function requires_schema_update(){
		$update_required = false;
		$site_schema_version = '';
		$site_config = $this->get_config();

		if($site_config && !isset($site_config['wordkeeper']['schema-version'])){
			$update_required = true;
		}
		else if($site_config['wordkeeper']['schema-version'] < $this->wordkeeper_schema_version){
				$update_required = true;
			}
		return $update_required;
	}


	/**
	 * upgrade_site_config function.
	 *
	 * @access public
	 * @return void
	 */
	public function upgrade_site_config(){
		$settings = $this->get_site_config();
		$templatePath = plugin_dir_path( __FILE__ ) . '../wordkeeper-speed-config.tpl';
		$template = "";

		if(file_exists($templatePath)){
			$template = file_get_contents($templatePath);
			foreach($settings as $setting => $v){
				$value = NULL;
				if(is_bool($v)){
					$value = $v ? "true" : "false";
				}
				else if($setting == 'schema-version'){
						$value = $this->wordkeeper_schema_version;
					}
				else{
					$value = $v;
				}
				$template = str_replace('['.$setting.']', $value, $template);
			}
			$template = str_replace('[schema-version]', $this->wordkeeper_schema_version, $template);
			$file = $this->get_config_path() . '/config.php';
			if(!is_dir($this->get_config_path())){
				mkdir($this->get_config_path(), 0775, true);
			}

			// Check for any new fields which were not replaced and replace them.
			$default_settings = $this->get_default_config();

			foreach($default_settings as $ds => $dv){
				if(strpos($template, '[' . $ds . ']') !== false){
					$value = NULL;
					if(is_bool($dv)){
						$value = $dv ? "true" : "false";
					}
					else{
						$value = $dv;
					}
					// New setting found which needs to be replaced
					$template = str_replace('['.$ds.']', $value, $template);
				}
			}

			file_put_contents($file, $template);
			return true;
		}
	}

	/**
	 * Updates the config array
	 *
	 * @access public
	 * @param mixed $key
	 * @param mixed $value
	 * @return void
	 */
	public function update_config($key, $value) {
		$this->wordkeeper[$key] = $value;
	}

	/**
	 * Updates the filtered array
	 *
	 * @access public
	 * @param mixed $key
	 * @param mixed $value
	 * @return void
	 */
	public function update_filtered($key, $value) {
		$this->wordkeeper_filtered[$key] = $value;
	}
}
