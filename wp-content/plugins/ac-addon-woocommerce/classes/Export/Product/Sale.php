<?php

namespace ACA\WC\Export\Product;

use ACA\WC\Column;
use ACP;
use WC_Product;

/**
 * @since 3.0
 *
 * @property Column\Product\Sale $column
 */
class Sale extends ACP\Export\Model {

	public function __construct( Column\Product\Sale $column ) {
		parent::__construct( $column );
	}

	public function get_value( $id ) {
		$product = wc_get_product( $id );

		if ( $this->column->is_scheduled( $product ) ) {
			return $product->get_date_on_sale_from( 'edit' )->format( 'Y-m-d' ) . ' / ' . $product->get_date_on_sale_to( 'edit' )->format( 'Y-m-d' );
		}

		return $product->is_on_sale( 'edit' );
	}

}