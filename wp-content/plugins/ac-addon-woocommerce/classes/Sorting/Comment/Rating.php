<?php

namespace ACA\WC\Sorting\Comment;

use ACP;

class Rating extends ACP\Sorting\Model\Meta {

	public function get_sorting_vars() {
		$key = $this->column->get_meta_key();

		$id = uniqid();

		$vars = array(
			'meta_query' => array(
				$id => array(
					'key'     => $key,
					'type'    => $this->get_data_type(),
					'value'   => '',
					'compare' => '!=',
				),
			),
			'orderby'    => $id,
		);

		return $vars;
	}

}