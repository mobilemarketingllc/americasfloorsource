<?php

namespace ACA\WC\Sorting\ShopOrder;

use ACA\WC\Column;
use ACP;

/**
 * @property Column\ShopOrder\Product $column
 */
class Product extends ACP\Sorting\Model {

	public function __construct( Column\ShopOrder\Product $column ) {
		parent::__construct( $column );
	}

	public function get_sorting_vars() {
		$values = array();

		foreach ( $this->strategy->get_results() as $order_id ) {
			$value = false;

			$product_ids = ac_addon_wc_helper()->get_product_ids_by_order( $order_id );

			if ( $product_ids ) {
				$value = $this->column->get_setting( 'post' )->format( $product_ids[0], $product_ids[0] );
			}

			$values[ $order_id ] = $value;
		}

		return array(
			'ids' => $this->sort( $values ),
		);

	}

}