<?php

namespace ACA\WC\Sorting\ShopCoupon;

use ACP;

class ExpiryDate extends ACP\Sorting\Model\Meta {

	public function __construct( $column ) {
		parent::__construct( $column );

		$this->set_data_type( 'date' );
	}

}