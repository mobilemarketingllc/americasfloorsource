<?php

namespace ACA\WC\Sorting\Product;

use ACA\WC\Column;
use ACP;

/**
 * @property Column\Product\Variation $column
 */
class Variation extends ACP\Sorting\Model {

	public function __construct( Column\Product\Variation $column ) {
		parent::__construct( $column );
	}

	public function get_sorting_vars() {
		$ids = array();
		foreach ( $this->strategy->get_results() as $product_id ) {
			$ids[ $product_id ] = count( $this->column->get_variation_ids( $product_id ) );
		}

		return array(
			'ids' => $this->sort( $ids ),
		);
	}

}