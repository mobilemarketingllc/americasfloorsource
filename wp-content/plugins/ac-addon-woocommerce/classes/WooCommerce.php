<?php

namespace ACA\WC;

use AC;

final class WooCommerce extends AC\Plugin {

	/**
	 * @var string
	 */
	protected $file;

	public function __construct( $file ) {
		$this->file = $file;
	}

	protected function get_file() {
		return $this->file;
	}

	protected function get_version_key() {
		return 'aca_wc_version';
	}

	/**
	 * Register hooks
	 */
	public function register() {
		add_action( 'ac/list_screen_groups', array( $this, 'register_list_screen_groups' ) );
		add_action( 'ac/column_groups', array( $this, 'register_column_groups' ) );
		add_action( 'ac/list_screens', array( $this, 'register_list_screens' ) );
		add_action( 'ac/column_types', array( $this, 'register_columns' ) );
		add_action( 'init', array( $this, 'install' ) );

		new TableScreen();

		$ajax_helper = new Rounding();
		$ajax_helper->register();

		// Variation List Table
		if ( $this->is_wc_version_gte( '3.3' ) ) {
			new PostType\ProductVariation();
		}
	}

	public function install() {
		parent::install();

		if ( $this->is_version_gte( '3.3' ) ) {
			$update = new Update\V3300();

			if ( ! $update->is_applied() ) {
				$update->run();
			}
		}
	}

	/**
	 * Register list screens
	 */
	public function register_list_screens() {
		$list_screens = array(
			new ListScreen\ShopOrder,
			new ListScreen\ShopCoupon,
			new ListScreen\Product,
			new ListScreen\ProductCategory,
		);

		if ( $this->is_wc_version_gte( '3.3' ) ) {
			$list_screens[] = new ListScreen\ProductVariation;
		}

		foreach ( $list_screens as $list_screen ) {
			AC()->register_list_screen( $list_screen );
		}
	}

	/**
	 * @param AC\ListScreen $list_screen
	 *
	 * @throws \ReflectionException
	 */
	public function register_columns( $list_screen ) {
		if ( $list_screen instanceof AC\ListScreen\User ) {
			$list_screen->register_column_types_from_dir( __NAMESPACE__ . '\Column\User' );
		}

		if ( $list_screen instanceof AC\ListScreen\Comment ) {
			$list_screen->register_column_types_from_dir( __NAMESPACE__ . '\Column\Comment' );
		}
	}

	/**
	 * @param AC\Groups $groups
	 */
	public function register_list_screen_groups( $groups ) {
		$groups->register_group( 'woocommerce', 'WooCommerce', 7 );
	}

	/**
	 * @param AC\Groups $groups
	 */
	public function register_column_groups( $groups ) {
		$groups->register_group( 'woocommerce', __( 'WooCommerce', 'codepress-admin-columns' ), 15 );
	}

	/**
	 * @param string $version
	 *
	 * @return bool
	 */
	public function is_wc_version_gte( $version ) {
		return version_compare( WC()->version, $version, '>=' );
	}

}