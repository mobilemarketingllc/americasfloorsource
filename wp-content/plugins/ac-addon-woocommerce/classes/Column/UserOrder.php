<?php

namespace ACA\WC\Column;

use AC;
use ACP;

/**
 * @since 3.0
 */
abstract class UserOrder extends AC\Column
	implements ACP\Sorting\Sortable {

	public function __construct() {
		$this->set_group( 'woocommerce' );
	}

	/**
	 * @param int $id
	 *
	 * @return int[]
	 */
	abstract protected function get_order_by_user_id( $id );

	/**
	 * @param int $id
	 *
	 * @return array|string
	 */
	public function get_raw_value( $id ) {
		return $this->get_order_by_user_id( $id );
	}

	/**
	 * @param int   $user_id
	 * @param array $args
	 *
	 * @return int|false
	 */
	protected function get_order_for_user( $user_id, array $args ) {
		$args = wp_parse_args( $args, array(
			'fields'         => 'ids',
			'post_type'      => 'shop_order',
			'posts_per_page' => 1,
			'post_status'    => 'wc-completed',
			'meta_query'     => array(
				array(
					'key'   => '_customer_user',
					'value' => $user_id,
				),
			),
		) );

		$order_ids = get_posts( $args );

		if ( ! $order_ids ) {
			return false;
		}

		return $order_ids[0];
	}

	public function sorting() {
		$model = new ACP\Sorting\Model( $this );
		$model->set_data_type( 'numeric' );

		return $model;
	}

}