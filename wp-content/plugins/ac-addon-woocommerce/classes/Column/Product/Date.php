<?php

namespace ACA\WC\Column\Product;

use AC;
use ACP;

/**
 * @since 2.0
 */
class Date extends AC\Column
	implements ACP\Filtering\Filterable, ACP\Search\Searchable {

	public function __construct() {
		$this->set_type( 'date' )
		     ->set_original( true );
	}

	public function filtering() {
		return new ACP\Filtering\Model\Post\Date( $this );
	}

	public function search() {
		return new ACP\Search\Comparison\Post\Date\PostDate();
	}

}