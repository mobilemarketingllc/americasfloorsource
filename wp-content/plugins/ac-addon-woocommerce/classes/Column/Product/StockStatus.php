<?php

namespace ACA\WC\Column\Product;

use AC;
use ACA\WC\Editing;
use ACA\WC\Filtering;
use ACP;

/**
 * @since 1.0
 */
class StockStatus extends AC\Column\Meta
	implements ACP\Sorting\Sortable, ACP\Editing\Editable, ACP\Filtering\Filterable, ACP\Search\Searchable {

	public function __construct() {
		$this->set_type( 'column-wc-stock-status' );
		$this->set_label( __( 'Stock Status', 'woocommerce' ) );
		$this->set_group( 'woocommerce' );
	}

	// Meta

	public function get_meta_key() {
		return '_stock_status';
	}

	// Display

	public function get_value( $post_id ) {
		$product = wc_get_product( $post_id );

		if ( ! $product ) {
			return false;
		}

		switch ( $this->get_raw_value( $post_id ) ) {

			case 'instock' :
				$value = ac_helper()->icon->yes( __( 'In stock', 'codepress-admin-columns' ) );

				break;
			case 'outofstock' :
				$value = ac_helper()->icon->no( __( 'Out of stock', 'codepress-admin-columns' ) );

				break;

			default :
				$value = $this->get_empty_char();
		}

		return $value;
	}

	public function get_raw_value( $post_id ) {
		return wc_get_product( $post_id )->get_stock_status();
	}

	// Pro

	public function sorting() {
		return new ACP\Sorting\Model( $this );
	}

	public function editing() {
		return new Editing\Product\Stock( $this );
	}

	public function filtering() {
		return new Filtering\Product\StockStatus( $this );
	}

	public function search() {
		return new \ACA\WC\Search\Product\StockStatus();
	}

	// Common

	public function get_supported_types() {
		return array( 'simple' );
	}

}