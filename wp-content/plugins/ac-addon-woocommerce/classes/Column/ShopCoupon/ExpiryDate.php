<?php

namespace ACA\WC\Column\ShopCoupon;

use AC;
use ACA\WC\Editing;
use ACA\WC\Search;
use ACA\WC\Sorting;
use ACP;

/**
 * @since 1.0
 */
class ExpiryDate extends AC\Column\Meta
	implements ACP\Sorting\Sortable, ACP\Editing\Editable, ACP\Filtering\Filterable, ACP\Search\Searchable {

	public function __construct() {
		$this->set_type( 'expiry_date' )
		     ->set_original( true );
	}

	public function get_meta_key() {
		return 'expiry_date';
	}

	public function get_value( $id ) {
		return null;
	}

	public function sorting() {
		return new Sorting\ShopCoupon\ExpiryDate( $this );
	}

	public function editing() {
		return new Editing\ShopCoupon\ExpiryDate( $this );
	}

	public function filtering() {
		return new ACP\Filtering\Model\MetaDate( $this );
	}

	public function search() {
		return new Search\Meta\Date\Ymd( $this->get_meta_key(), AC\MetaType::POST );
	}

}