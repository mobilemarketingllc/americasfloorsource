<?php

namespace ACA\WC\Column\User;

use AC;
use ACA\WC\Export;
use ACA\WC\Settings;
use ACP;

/**
 * @since 3.0
 */
class Ratings extends AC\Column
	implements ACP\Sorting\Sortable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'column-wc-user-ratings' );
		$this->set_label( __( 'Ratings', 'woocommerce' ) );
		$this->set_group( 'woocommerce' );
	}

	public function register_settings() {
		$this->add_setting( new Settings\User\Ratings( $this ) );
	}

	// Display

	public function get_raw_value( $user_id ) {
		global $wpdb;

		$display = $this->get_setting( 'user_ratings' )->get_value();
		$is_avg = $display === 'avg';
		$af = $is_avg ? 'AVG' : 'COUNT';

		$sql = "
			SELECT {$af}(cm.meta_value)
			FROM {$wpdb->comments} AS c
			INNER JOIN {$wpdb->posts} AS p 
				ON c.comment_post_ID = p.ID 
				AND p.post_type = 'product'
			INNER JOIN {$wpdb->commentmeta} AS cm 
				ON cm.comment_id = c.comment_ID
			WHERE c.user_id = %d
			AND c.comment_approved = 1
			AND cm.meta_key = 'rating'
		";

		$stmt = $wpdb->prepare( $sql, array( $user_id ) );
		$value = $wpdb->get_var( $stmt );

		if ( $is_avg ) {
			$value = round( $value, 3 );
		}

		return $value;
	}

	// Pro

	public function export() {
		return new ACP\Export\Model\RawValue( $this );
	}

	public function sorting() {
		$sorting = new ACP\Sorting\Model( $this );
		$sorting->set_data_type( 'numeric' );

		return $sorting;
	}

}