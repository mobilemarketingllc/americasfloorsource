<?php

namespace ACA\WC\Column\User;

use AC;
use ACA\WC\Export;
use ACA\WC\Sorting;
use ACP;
use WC_Order;

/**
 * @since 1.3
 */
class Orders extends AC\Column
	implements ACP\Sorting\Sortable, ACP\Export\Exportable {

	public function __construct() {
		$this->set_type( 'column-wc-user-orders' );
		$this->set_label( __( 'Orders', 'woocommerce' ) );
		$this->set_group( 'woocommerce' );
	}

	public function get_value( $user_id ) {
		$order_ids = $this->get_raw_value( $user_id );

		if ( ! $order_ids ) {
			return $this->get_empty_char();
		}

		$values = array();

		foreach ( $order_ids as $order_id ) {
			$order = wc_get_order( $order_id );
			$status = $order->get_status();
			$values[ $status ][] = '<div class="order order-' . esc_attr( $status ) . '" ' . ac_helper()->html->get_tooltip_attr( $this->get_order_tooltip( $order ) ) . '">' . ac_helper()->html->link( get_edit_post_link( $order_id ), $order->get_order_number() ) . '</div>';
		}

		if ( ! $values ) {
			return $this->get_empty_char();
		}

		$output = '';

		foreach ( $values as $status => $orders ) {
			$output .= implode( '', $orders ) . "</br>";
		}

		return $output;
	}

	public function get_raw_value( $user_id ) {
		return ac_addon_wc_helper()->get_order_ids_by_user( $user_id, 'any' );
	}

	public function sorting() {
		return new Sorting\User\Orders( $this );
	}

	public function export() {
		return new Export\User\Orders( $this );
	}

	/**
	 * @param $order WC_Order
	 *
	 * @return string
	 */
	private function get_order_tooltip( $order ) {
		$tooltip = array(
			wc_get_order_status_name( $order->get_status() ),
		);

		$item_count = $order->get_item_count();

		if ( $item_count ) {
			$tooltip[] = $item_count . ' ' . __( 'items', 'codepress-admin-columns' );
		}

		$total = $order->get_total();

		if ( $total ) {
			$tooltip[] = get_woocommerce_currency_symbol( $order->get_currency() ) . wc_trim_zeros( number_format( $total, 2 ) );
		}

		$tooltip[] = date_i18n( get_option( 'date_format' ), strtotime( $order->get_date_created() ) );

		return implode( ' | ', $tooltip );
	}

}