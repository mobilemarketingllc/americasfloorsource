<?php

namespace ACA\WC\Column\User;

use AC;
use ACA\WC\Search;
use ACP;

/**
 * @since 1.3
 */
class OrderCount extends AC\Column
	implements ACP\Sorting\Sortable, ACP\Search\Searchable {

	public function __construct() {
		$this->set_type( 'column-wc-user-order_count' );
		$this->set_label( __( 'Number of Orders', 'woocommerce' ) );
		$this->set_group( 'woocommerce' );
	}

	// Display

	public function get_value( $user_id ) {
		return $this->get_raw_value( $user_id );
	}

	public function get_raw_value( $user_id ) {
		return count( ac_addon_wc_helper()->get_order_ids_by_user( $user_id, 'any' ) );
	}

	// Pro

	public function sorting() {
		return new ACP\Sorting\Model( $this );
	}

	public function search() {
		return new Search\User\OrderCount();
	}

}