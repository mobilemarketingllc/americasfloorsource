<?php

namespace ACA\WC\Column\User;

use ACA\WC\Column;
use ACA\WC\Settings;

/**
 * @since 3.0
 */
class FirstOrder extends Column\UserOrder {

	public function __construct() {
		parent::__construct();

		$this->set_type( 'column-wc-user-first_order' );
		$this->set_label( __( 'First Order', 'codepress-admin-columns' ) );
	}

	public function get_order_by_user_id( $id ) {
		return $this->get_order_for_user( $id, array( 'order' => 'ASC' ) );
	}

	public function register_settings() {
		$this->add_setting( new Settings\User\Order( $this ) );
	}

}