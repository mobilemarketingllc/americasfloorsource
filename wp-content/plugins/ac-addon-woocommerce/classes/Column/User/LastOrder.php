<?php

namespace ACA\WC\Column\User;

use ACA\WC\Column;
use ACA\WC\Settings;

/**
 * @since 3.0
 */
class LastOrder extends Column\UserOrder {

	public function __construct() {
		parent::__construct();

		$this->set_type( 'column-wc-user-last_order' );
		$this->set_label( __( 'Last Order', 'codepress-admin-columns' ) );
	}

	public function get_order_by_user_id( $id ) {
		return $this->get_order_for_user( $id, array( 'order' => 'DESC' ) );
	}

	public function register_settings() {
		$this->add_setting( new Settings\User\Order( $this ) );
	}

}