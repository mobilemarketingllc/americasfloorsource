<?php

namespace ACA\WC\Column\ShopOrder;

use AC;
use ACA\WC\Export;
use ACA\WC\Field\ShopOrder;
use ACA\WC\Settings;
use ACA\WC\Sorting;
use ACP;

/**
 * @since 3.0
 */
class OrderDate extends AC\Column\Meta
	implements ACP\Export\Exportable, ACP\Sorting\Sortable, ACP\Filtering\Filterable, ACP\Search\Searchable {

	/**
	 * @var ShopOrder\OrderDate
	 */
	private $field;

	public function __construct() {
		$this->set_label( 'Date' );
		$this->set_type( 'column-wc-order_date' );
		$this->set_group( 'woocommerce' );
	}

	public function register_settings() {
		$this->add_setting( new Settings\ShopOrder\OrderDate( $this ) );
	}

	public function get_meta_key() {
		if ( ! $this->get_field() ) {
			return false;
		}

		return $this->get_field()->get_meta_key();
	}

	public function export() {
		if ( ! $this->get_field() ) {
			return new ACP\Export\Model\Disabled( $this );
		}

		return $this->get_field()->export();
	}

	public function sorting() {
		if ( ! $this->get_field() ) {
			return new ACP\Sorting\Model\Disabled( $this );
		}

		return $this->get_field()->sorting();
	}

	public function filtering() {
		if ( ! $this->get_field() ) {
			return new ACP\Filtering\Model\Disabled( $this );
		}

		return $this->get_field()->filtering();
	}

	public function search() {
		if ( ! $this->get_field() ) {
			return false;
		}

		return $this->get_field()->search();
	}

	private function set_field() {
		$type = $this->get_setting( 'date_type' )->get_value();

		foreach ( $this->get_fields() as $field ) {
			/** @var ShopOrder\OrderDate $field */
			if ( $field->get_key() === $type ) {
				$this->field = $field;
			}
		}
	}

	/**
	 * @return ShopOrder\OrderDate|false
	 */
	public function get_field() {
		if ( null === $this->field ) {
			$this->set_field();
		}

		return $this->field;
	}

	/**
	 * @return OrderDate[]
	 */
	public function get_fields() {
		$classes = AC\Autoloader::instance()->get_class_names_from_dir( 'ACA\WC\Field\ShopOrder\OrderDate' );
		$fields = array();

		foreach ( $classes as $class ) {
			$fields[] = new $class( $this );
		}

		return $fields;
	}

}