<?php

namespace ACA\WC\Column\ShopOrder;

use AC;
use AC\Collection;
use ACA\WC\Filtering;
use ACA\WC\Search;
use ACA\WC\Settings;
use ACA\WC\Sorting;
use ACP;

/**
 * @since 1.3.1
 */
class Product extends AC\Column
	implements ACP\Sorting\Sortable, ACP\Filtering\Filterable, ACP\Export\Exportable, ACP\Search\Searchable {

	public function __construct() {
		$this->set_group( 'woocommerce' );
		$this->set_type( 'column-wc-product' );
		$this->set_label( __( 'Product', 'woocommerce' ) );
	}

	public function get_raw_value( $order_id ) {
		return new Collection( ac_addon_wc_helper()->get_product_or_variation_ids_by_order( $order_id ) );
	}

	public function filtering() {
		if ( in_array( $this->get_product_property(), array( 'title', 'sku' ) ) ) {
			return new Filtering\ShopOrder\Product( $this );
		}

		return new ACP\Filtering\Model\Disabled( $this );
	}

	public function sorting() {
		if ( 'custom_field' === $this->get_product_property() ) {
			return new ACP\Sorting\Model\Disabled( $this );
		}

		return new Sorting\ShopOrder\Product( $this );
	}

	public function export() {
		return new ACP\Export\Model\StrippedValue( $this );
	}

	public function search() {
		return new Search\ShopOrder\Product();
	}

	public function register_settings() {
		$this->add_setting( new Settings\ShopOrder\Product( $this ) );
	}

	public function get_product_property() {
		return $this->get_setting( 'post' )->get_value();
	}

}