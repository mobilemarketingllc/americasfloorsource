<?php

namespace ACA\WC\Column\ShopOrder;

use AC;
use ACA\WC\Filtering;
use ACP;
use WC_Order;

/**
 * @since 1.4
 */
class ShippingMethod extends AC\Column
	implements ACP\Sorting\Sortable, ACP\Filtering\Filterable, ACP\Export\Exportable, ACP\Search\Searchable {

	public function __construct() {
		$this->set_group( 'woocommerce' );
		$this->set_type( 'column-wc-order_shipping_method' );
		$this->set_label( __( 'Shipping Method', 'woocommerce' ) );
	}

	public function get_value( $order_id ) {
		$order = wc_get_order( $order_id );

		$value = $order->get_shipping_method();

		if ( ! $value ) {
			return $this->get_empty_char();
		}

		return $value;
	}

	public function filtering() {
		return new Filtering\ShopOrder\ShippingMethod( $this );
	}

	public function sorting() {
		return new ACP\Sorting\Model( $this );
	}

	public function search() {
		return new \ACA\WC\Search\ShopOrder\ShippingMethod();
	}

	public function export() {
		return new ACP\Export\Model\StrippedValue( $this );
	}

}