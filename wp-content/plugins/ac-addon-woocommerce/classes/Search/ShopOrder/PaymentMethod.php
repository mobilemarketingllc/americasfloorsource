<?php

namespace ACA\WC\Search\ShopOrder;

use AC;
use AC\MetaType;
use ACA\WC\Helper\Select;
use ACP\Search\Comparison;
use ACP\Search\Operators;

class PaymentMethod extends Comparison\Meta implements Comparison\Values {

	public function __construct() {
		$operators = new Operators( array(
			Operators::EQ,
		) );

		parent::__construct( $operators, '_payment_method_title', MetaType::POST );
	}

	public function get_values() {
		$options = array();

		foreach ( WC()->payment_gateways()->payment_gateways() as $gateway ) {

			/* @var \WC_Payment_Gateway $gateway */
			if ( 'yes' === $gateway->enabled ) {
				$options[ $gateway->get_title() ] = $gateway->get_title();
			}
		}

		return AC\Helper\Select\Options::create_from_array( $options );
	}

}