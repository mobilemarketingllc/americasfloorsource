<?php

namespace ACA\WC\Search\ShopOrder;

use AC;
use ACA\WC\Helper\Select;
use ACP;
use ACP\Search\Comparison;
use ACP\Search\Query\Bindings;
use ACP\Search\Value;

class Product extends Comparison
	implements Comparison\SearchableValues {

	public function __construct() {
		$operators = new ACP\Search\Operators(
			array(
				ACP\Search\Operators::EQ,
			)
		);

		parent::__construct( $operators );
	}

	protected function create_query_bindings( $operator, Value $value ) {
		$bindings = new Bindings();

		return $bindings->where( $this->get_where( $value->get_value() ) );
	}

	/**
	 * @param int $product_id
	 *
	 * @return string
	 */
	public function get_where( $product_id ) {
		global $wpdb;
		$orders = $this->get_orders_ids_by_product_id( $product_id );

		if ( empty( $orders ) ) {
			$orders = array( 0 );
		}

		return sprintf( "{$wpdb->posts}.ID IN( %s )", implode( ',', $orders ) );
	}

	public function get_values( $s, $paged ) {
		$entities = new Select\Entities\Product( array(
			's'         => $s,
			'paged'     => $paged,
			'post_type' => array( 'product', 'product_variation' ),
		) );

		return new AC\Helper\Select\Options\Paginated(
			$entities,
			new Select\Formatter\ProductTitleAndSKU( $entities )
		);
	}

	/**
	 * Get All orders IDs for a given product ID.
	 *
	 * @param  integer $product_id
	 *
	 * @return array
	 */
	private function get_orders_ids_by_product_id( $product_id ) {
		global $wpdb;

		$results = $wpdb->get_col( $wpdb->prepare( "
	        SELECT order_items.order_id
	        FROM {$wpdb->prefix}woocommerce_order_items as order_items
	        LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta as order_item_meta ON order_items.order_item_id = order_item_meta.order_item_id
	        LEFT JOIN {$wpdb->posts} AS posts ON order_items.order_id = posts.ID
	        WHERE posts.post_type = 'shop_order'
	        AND order_items.order_item_type = 'line_item'
	        AND ( order_item_meta.meta_key = '_product_id' OR order_item_meta.meta_key = '_variation_id' )
	        AND order_item_meta.meta_value = %s
        ", $product_id ) );

		return $results;
	}

}