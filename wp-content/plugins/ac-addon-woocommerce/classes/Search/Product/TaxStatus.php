<?php

namespace ACA\WC\Search\Product;

use AC\MetaType;
use ACA\WC\Helper\Select;
use ACP\Search\Comparison;
use ACP\Search\Operators;

class TaxStatus extends Comparison\Meta {

	/** @var array */
	private $statuses;

	public function __construct( $statuses ) {
		$operators = new Operators( array(
			Operators::EQ,
		) );

		$this->statuses = $statuses;

		parent::__construct( $operators, '_tax_status', MetaType::POST );
	}

}