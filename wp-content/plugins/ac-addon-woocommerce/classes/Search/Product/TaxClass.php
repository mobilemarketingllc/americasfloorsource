<?php

namespace ACA\WC\Search\Product;

use AC\MetaType;
use ACA\WC\Helper\Select;
use ACP\Search\Comparison;
use ACP\Search\Operators;

class TaxClass extends Comparison\Meta {

	/** @var array */
	private $tax_classes;

	public function __construct( $tax_classes ) {
		$operators = new Operators( array(
			Operators::EQ,
		) );

		$this->tax_classes = $tax_classes;

		parent::__construct( $operators, '_tax_class', MetaType::POST );
	}

}