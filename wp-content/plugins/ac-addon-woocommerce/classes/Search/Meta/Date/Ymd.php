<?php

namespace ACA\WC\Search\Meta\Date;

use ACA\WC\Helper\Select;
use ACA\WC\Search\Meta;
use ACP\Search\Value;

class Ymd extends Meta\Date {

	protected function get_date_format() {
		return 'Y-m-d';
	}

	protected function get_meta_value_type() {
		return Value::DATE;
	}

}