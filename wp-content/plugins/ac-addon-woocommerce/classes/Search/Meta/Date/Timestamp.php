<?php

namespace ACA\WC\Search\Meta\Date;

use ACA\WC\Helper\Select;
use ACA\WC\Search\Meta;
use ACP\Search\Value;

class Timestamp extends Meta\Date {

	protected function get_date_format() {
		return 'U';
	}

	protected function get_meta_value_type() {
		return Value::INT;
	}

}