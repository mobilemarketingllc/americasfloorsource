<?php

namespace ACA\WC\Search\Meta;

use ACA\WC\Helper\Select;
use ACP\Search\Comparison;
use ACP\Search\Labels;
use ACP\Search\Operators;
use ACP\Search\Value;
use DateTime;

abstract class Date extends Comparison\Meta {

	abstract protected function get_date_format();

	abstract protected function get_meta_value_type();

	public function __construct( $meta_key, $meta_type ) {
		$operators = new Operators( array(
			Operators::EQ,
			Operators::GT,
			Operators::LT,
			Operators::BETWEEN,
			Operators::IS_EMPTY,
			Operators::NOT_IS_EMPTY,
		) );

		parent::__construct( $operators, $meta_key, $meta_type, Value::DATE, new Labels\Date() );
	}

	private function transform_date( $date ) {
		$date = DateTime::createFromFormat( 'Y-m-d', $date );

		return $date->format( $this->get_date_format() );
	}

	private function format_value( Value $value ) {
		$date = $value->get_value();

		if ( is_array( $date ) ) {
			$date = array_map( array( $this, 'transform_date' ), $date );
		} else {
			$date = $this->transform_date( $date );
		}

		return new Value(
			$date,
			$this->get_meta_value_type()
		);
	}

	protected function get_meta_query( $operator, Value $value ) {
		if ( Operators::EQ === $operator ) {
			$date = new DateTime( $value->get_value() );
			$value = new Value(
				array(
					$date->format( 'Y-m-d' ),
					$date->modify( '+1 day' )->format( 'Y-m-d' ),
				),
				Value::DATE
			);
			$operator = 'BETWEEN';
		}

		$value = $this->format_value( $value );

		return parent::get_meta_query( $operator, $value );
	}

}