<?php

namespace ACA\WC\PostType;

use ACA\WC\ListTable;

/**
 * @since 3.0
 */
class ProductVariation {

	private $post_type = 'product_variation';

	/**
	 * Constructor.
	 */
	public function __construct() {
		// Menu
		add_filter( 'woocommerce_register_post_type_' . $this->post_type, array( $this, 'enable_variation_list_table' ), 10, 2 );
		add_action( 'admin_menu', array( $this, 'admin_menu' ) );

		// Load correct list table classes for current screen.
		add_action( 'current_screen', array( $this, 'setup_screen' ) );
		add_action( 'check_ajax_referer', array( $this, 'setup_screen' ) );
	}

	/**
	 * @param array $args
	 *
	 * @return array
	 */
	public function enable_variation_list_table( $args ) {
		$args['show_ui'] = true;
		$args['show_in_menu'] = true;

		if ( ! isset( $args['capabilities'] ) ) {
			$args['capabilities'] = array();
		}

		$args['capabilities']['create_posts'] = 'do_not_allow';
		$args['capabilities']['delete_posts'] = true;

		if ( ! isset( $args['labels'] ) ) {
			$args['labels'] = array(
				'name'               => __( 'Product Variations', 'codepress-admin-columns' ),
				'singular_name'      => __( 'Product Variation', 'codepress-admin-columns' ),
				'all_items'          => __( 'Product Variations', 'codepress-admin-columns' ),
				'add_new'            => __( 'Add New', 'codepress-admin-columns' ),
				'add_new_item'       => __( 'Add new variation', 'codepress-admin-columns' ),
				'edit'               => __( 'Edit', 'codepress-admin-columns' ),
				'edit_item'          => __( 'Edit variation', 'codepress-admin-columns' ),
				'new_item'           => __( 'New variation', 'codepress-admin-columns' ),
				'view'               => __( 'View variation', 'codepress-admin-columns' ),
				'view_item'          => __( 'View variation', 'codepress-admin-columns' ),
				'search_items'       => __( 'Search variations', 'woocommerce' ),
				'not_found'          => __( 'No variations found', 'codepress-admin-columns' ),
				'not_found_in_trash' => __( 'No variations found in trash', 'codepress-admin-columns' ),
				'insert_into_item'   => __( 'Insert into variation', 'codepress-admin-columns' ),
			);
		}

		return $args;
	}

	/**
	 * Place variations menu under products
	 */
	public function admin_menu() {
		global $submenu;

		$slug_variation = 'edit.php?post_type=' . $this->post_type;

		if ( ! isset( $submenu[ $slug_variation ] ) ) {
			return;
		}

		$variation = reset( $submenu[ $slug_variation ] );

		$slug_product = 'edit.php?post_type=product';

		if ( ! isset( $submenu[ $slug_product ] ) ) {
			return;
		}

		// Place on first available position after position x
		for ( $pos = 10; $pos < 100; $pos++ ) {
			if ( isset( $submenu[ $slug_product ][ $pos ] ) ) {
				continue;
			}

			$submenu[ $slug_product ][ $pos ] = $variation;
			break;
		}

		ksort( $submenu[ $slug_product ] );

		remove_menu_page( $slug_variation );
	}

	/**
	 * Load List Table
	 */
	public function setup_screen() {
		if ( 'edit-' . $this->post_type === $this->get_current_screen_id() ) {
			new ListTable\ProductVariation();
		}
	}

	/**
	 * @return false|string
	 */
	private function get_current_screen_id() {
		$screen_id = false;

		if ( function_exists( 'get_current_screen' ) ) {
			$screen = get_current_screen();
			$screen_id = isset( $screen, $screen->id ) ? $screen->id : '';
		}

		if ( AC()->is_doing_ajax() && ! empty( $_REQUEST['screen'] ) ) {
			$screen_id = wc_clean( wp_unslash( $_REQUEST['screen'] ) );
		}

		return $screen_id;
	}

}