<?php

namespace ACA\WC\Editing\ShopCoupon;

use ACP;

class Description extends ACP\Editing\Model\Post {

	public function get_view_settings() {
		return array(
			'type' => 'textarea',
		);
	}

	public function save( $id, $value ) {
		return $this->update_post( $id, array( 'post_excerpt' => $value ) );
	}

}