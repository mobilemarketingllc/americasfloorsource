<?php

namespace ACA\WC\Editing\Product;

use ACP;

/**
 * @since 3.0
 */
class ShortDescription extends ACP\Editing\Model\Post\Excerpt {

	public function get_view_settings() {
		return array(
			'type' => 'textarea',
		);
	}

}