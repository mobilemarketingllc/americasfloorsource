<?php

namespace ACA\WC\Field\ShopOrder\OrderDate;

use ACA\WC\Field\ShopOrder\OrderDate;
use ACA\WC\Filtering;
use ACA\WC\Search;
use WC_Order;

/**
 * @since 3.0
 */
class Completed extends OrderDate {

	public function set_label() {
		$this->label = __( 'Completed', 'codepress-admin-columns' );
	}

	public function get_date( WC_Order $order ) {
		return $order->get_date_completed();
	}

	public function get_meta_key() {
		return '_date_completed';
	}

	public function filtering() {
		return new Filtering\ShopOrder\MetaDate( $this->column );
	}

	public function search() {
		return new Search\Meta\Date\Timestamp( $this->get_meta_key(), 'post' );
	}

}