<?php
/*
Plugin Name: 	    Admin Columns Pro - WooCommerce
Version: 		    3.3.7
Description: 	    Powerful columns for the WooCommerce Product, Orders and Coupon overview screens
Author: 		    Admin Columns
Author URI: 	    https://www.admincolumns.com
Plugin URI: 	    https://www.admincolumns.com
Text Domain: 		codepress-admin-columns
WC tested up to:    3.7
*/

use AC\Autoloader;
use ACA\WC\Dependencies;
use ACA\WC\WooCommerce;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! is_admin() ) {
	return;
}

require_once __DIR__ . '/classes/Dependencies.php';

add_action( 'after_setup_theme', function () {
	$dependencies = new Dependencies( plugin_basename( __FILE__ ), '3.3.7' );
	$dependencies->requires_acp( '4.7.1' );
	$dependencies->requires_php( '5.3.6' );

	if ( ! class_exists( 'WooCommerce', false ) ) {
		$dependencies->add_missing_plugin( 'WooCommerce', $dependencies->get_search_url( 'WooCommerce' ) );
	}

	if ( $dependencies->has_missing() ) {
		return;
	}

	Autoloader::instance()->register_prefix( 'ACA\WC', __DIR__ . '/classes/' );

	$addon = new WooCommerce( __FILE__ );
	$addon->register();
} );

function ac_addon_wc() {
	return new WooCommerce( __FILE__ );
}

function ac_addon_wc_helper() {
	return new ACA\WC\Helper();
}