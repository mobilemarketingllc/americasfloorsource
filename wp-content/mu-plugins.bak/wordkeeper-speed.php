<?php
/**
 *
 * @link              https://wordkeeper.com
 * @since             1.0.0
 * @package           WordKeeper Speed Optimization
 *
 * @wordpress-plugin
 * Plugin Name:       WordKeeper Speed Optimization
 * Plugin URI:        https://wordkeeper.com/plugins/optimization
 * Description:       WordKeeper Speed Optimization
 * Version:           1.0.0
 * Author:            WordKeeper
 * Author URI:        https://wordkeeper.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wordkeeper-speed
 * Domain Path:       /languages
 *
 * Optimizes front end speed and server caching
 * Includes silent heartbeat controls
 * Includes silent resource hinting (as close as one can get to HTTP/2 server push)
 * Includes settings listed in config below
 */

// If this file is called directly, abort.
if(!defined('WPINC')) {
	die;
}

// WordKeeper settings array
$wordkeeper = array(
	'defer-javascript' => false,
	'disable-header-garbage' => true,
	'optimize-db-count-queries' => true,
	'remove-query-strings' => true,
	'disable-emojis' => true,
	'disable-wordpress-embeds' => true,
	'minify-js' => true,
	'minify-css' => true,
	'lazyload-images' => false,
	'lazyload-iframes' => false,
	'lazyload-mode' => 'near', // options (near, visible, none)
	'lazyload-videos' => true,
	'lazyload-videos-mode' => 'default',  // options (default:lightbox)
	'disable-maps-api' => false,
	'strip-duplicate-assets' => false,
	'strip-comments' => false,
	'strip-session-cookie' => false,
	'optimize-google-fonts' => false,
	'disable-woocommerce-refresh-fragments' => true,
	'use-deferscript' => false,
	'deferscript-delay' => '1500' // number of milliseconds to delay
);

$wordkeeper_overrides = array();
$wordkeeper_filtered = array();
$wordkeeper_map = array(
	'wordkeeper_process_buffer_images' => 'lazyload-images',
	'wordkeeper_process_buffer_videos' => 'lazyload-videos',
	'wordkeeper_process_buffer_iframes' => 'lazyload-iframes',
	'wordkeeper_lazyload_video' => 'lazyload-videos',
	'wordkeeper_lazyload_script_videos' => 'lazyload-videos',
	'wordkeeper_lazyload_image' => 'lazyload-images',
	'wordkeeper_lazyload_iframe' => 'lazyload-iframes',
	'wordkeeper_defer_buffer_scripts' => 'defer-javascript',
	'wordkeeper_apply_deferscript' => 'use-deferscript',
	'wordkeeper_strip_duplicate_assets' => 'strip-duplicate-assets',
	'wordkeeper_strip_session_cookie' => 'strip-session-cookie',
	'wordkeeper_strip_comments' => 'strip-comments',
	'wordkeeper_remove_google_maps' => 'disable-maps-api',
	'wordkeeper_concatenate_google_fonts' => 'optimize-google-fonts',
	'wordkeeper_remove_query_strings' => 'remove-query-strings'
);

$wordkeeper_active_plugins = get_option('active_plugins');
require dirname(__FILE__) . '/wordkeeper-speed/inc/exclusions.php';
unset($wordkeeper_active_plugins);

$wordkeeper['use-buffering'] = ($wordkeeper['defer-javascript'] || $wordkeeper['lazyload-iframes'] || $wordkeeper['lazyload-videos'] || $wordkeeper['disable-maps-api'] || $wordkeeper['strip-duplicate-assets'] || $wordkeeper['minify-js'] || $wordkeeper['minify-css']) ? true : false;
$wordkeeper['capture-scripts'] = ($wordkeeper['defer-javascript'] || $wordkeeper['disable-maps-api'] || $wordkeeper['deferscript-delay'] || $wordkeeper['minify-js']) ? true : false;
$wordkeeper['capture-iframes'] = ($wordkeeper['lazyload-iframes'] || $wordkeeper['lazyload-videos']) ? true : false;
$wordkeeper['capture-images'] = ($wordkeeper['lazyload-images']) ? true : false;
$wordkeeper['capture-links'] = ($wordkeeper['strip-duplicate-assets'] || $wordkeeper['minify-css']) ? true : false;

$urlparts = parse_url($_SERVER['REQUEST_URI']);
$urlparts = trim($urlparts['path'], '/');
$urlparts = explode('/', $urlparts);
$wordkeeper['amp'] = ($urlparts[count($urlparts) - 1] == 'amp') ? true : false;
unset($urlparts);

require(dirname(__FILE__) . '/wordkeeper-speed/wordkeeper-speed.php');
