<?php
	get_header();

	$filter_bg['component-filter'] = get_field('filter_bar_bg', 'option');

	$now = date("Y-m-d H:i:s", current_time('timestamp'));

	//Filter for Active Promotions
	$args = array(
		'post_type' => 'promotion',
		'posts_per_page' => -1,
		'order' => 'DESC',
		'orderby' => 'date',
		'meta_query' => array(
			'relation'	=> 'AND',
	        array(
	            'key' => 'promo_start',
	            'value' => $now,
	            'compare' => '<=',
	            'type' => 'DATE'
	        ),
	        array(
	            'key' => 'promo_end',
	            'value' => $now,
	            'compare' => '>=',
	            'type' => 'DATE'
	        )
	    )
	);

	$promotions = new WP_Query($args);

?>

<main id="main" role="main">

	<!-- Section: Header Large -->
	<?php include('components/header-skinny.php'); ?>

	<?php if ($promotions->have_posts()): ?>

		<!-- Section: Inspiration Grid -->
		<section class="afs-section afs-post-grid afs-promotions-grid">

			<div class="container inspirations-container">
				<div class="row">

					<div class="afs-post-grid-header afs-CAS-1 col-sm-12">
						<?php the_field("post_header"); ?>
					</div>

					<?php
						while($promotions->have_posts()) :
							$promotions->the_post();
							get_template_part( 'components/element', 'promotion-card' );
						endwhile;
					?>
				</div>
			</div>
		</section>

	<?php
		else :
			//No posts returned
			include('components/component-no-promo.php');

		endif;

		wp_reset_postdata()
	?>


	<!-- Section: Split Call to Action -->
	<?php include('components/component-financingcta.php'); ?>

	<!-- Section: Half Page content -->
	<?php include('components/component-inspirationcta.php'); ?>

</main>

<?php

	get_footer();

?>
