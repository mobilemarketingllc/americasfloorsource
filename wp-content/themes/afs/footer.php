<?php


	$bbb_images = get_field('footer_logos', 'options');
	// print_r($bbb_images);


?>

		<footer id="footer" class="afs-section afs-footer">
			<div class="container">
				<div class="footer-main row">
					<div class="main-logo col-lg-3">
						<a href="<?php bloginfo('url'); ?>">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/afs-logo.svg" alt="<?php bloginfo('name'); ?>" />
						</a>
					</div>

					<?php
						//Footer Site Map #1
						if (has_nav_menu('footer-sitemap-1')): ?>
						<div class="main-sitemap-1 col-lg-3 col-xs-6">
							<?php
								wp_nav_menu(array('theme_location' => 'footer-sitemap-1', 'walker' => new Walker_Nav_Menu));
							?>
						</div>
					<?php endif ?>

					<?php
						//Footer Site Map #2
						if (has_nav_menu('footer-sitemap-2')): ?>
						<div class="main-sitemap-2 col-lg-3 col-xs-3">
							<?php
								wp_nav_menu(array('theme_location' => 'footer-sitemap-2', 'walker' => new Walker_Nav_Menu));
							?>
						</div>
					<?php endif ?>

					<?php
						//Footer Locations
						if (has_nav_menu('footer-locations')): ?>
						<div class="main-locations col-lg-3 col-xs-3">
							<?php
								wp_nav_menu(array('theme_location' => 'footer-locations', 'walker' => new Walker_Nav_Menu));
							?>
						</div>
					<?php endif ?>

					<div class="main-display col-xl-3 col-lg-12">
						<div class="main-social">
							<h5 class="main-social-title">Stay connected with us on</h3>
							<?php social_links('list-inline'); ?>
						</div>


						<?php if (count($bbb_images)): ?>
							<div class="main-bbb">
								<?php foreach ($bbb_images as $key => $image): ?>
									<img src="<?php echo $image['sizes']['logo-sm']; ?>" alt="<?php echo $image['alt']; ?>">
								<?php endforeach ?>
							</div>
						<?php endif ?>


					</div>

				</div>

				<div class="footer-credits row">
					<div class="col-sm-12">
						<div class="copy">
							<?php echo '©' . date('Y') . ' ' . get_bloginfo('name'); ?>. All Rights Reserved.
							<ul class="list-inline">
								<li><a href="<?php echo get_bloginfo('url') . '/terms-of-use'; ?>">Terms of Use</a></li>
								<li><a href="<?php echo get_bloginfo('url') . '/privacy-policy'; ?>">Privacy Policy</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<?php wp_footer(); ?>

	</body>
</html>
