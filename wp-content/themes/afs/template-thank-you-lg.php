<?php

/*
	Template Name: Thank You - Large Header
*/

	get_header();


	$content = get_field("about_content");
	$sidebar = get_field("about_sidebar");
	$menu = get_field("about_menu");

	// print_r($content);


?>

<main id="main" role="main">

	<!-- Section: Header Large -->
	<?php include('components/header-large.php'); ?>

	<!-- Section: Content with Sidebar -->
	<section class="afs-section afs-about">
		<div class="container">
			<div class="row">
				<?php
					if ($menu && has_nav_menu('about-template') ) : ?>
						<div class="component-sidebar-menu col-lg-3 col-md-3 col-xs-12">
							<div class="dropdown">
								<button class="dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
									Navigation
								</button>
								<?php
									wp_nav_menu(array('theme_location' => 'about-template', 'menu_class' => 'dropdown-menu'));
								?>
							</div>
						</div>

				<?php
					endif;

					foreach ($content as $key => $section):

						switch ($section['acf_fc_layout']) {
							case 'layout_simple':
								include('components/component-layout-simple.php');

								break;

							case 'layout_simple_logos':
								include('components/component-layout-simple-logos.php');

								break;

							case 'layout_3col':
								include('components/component-layout-3col.php');

								break;

							case 'layout_locations':
								include('components/component-layout-locations.php');

								break;

						}

						if($key == 0 && $sidebar) {
							get_sidebar('about');
						}

					endforeach;
				?>
		</div>
	</section>

	<!-- Section: Testimonial -->
	<?php include('components/component-testimonial.php'); ?>

</main>

<?php

	get_footer();

?>
