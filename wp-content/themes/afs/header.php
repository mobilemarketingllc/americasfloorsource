<?php
	//Get variant number from cookie or set a new one.
	$abVariation = (!empty($_COOKIE['ab-variant'])) ? $_COOKIE['ab-variant'] : rand(0, 1);
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- A/B Testing variant information -->
		<script>
			document.variantID = <?php echo $abVariation; ?>;
			<?php if(empty($_COOKIE['ab-variant'])): ?>
				document.cookie = 'ab-variant=<?php echo $abVariation; ?>;path="/"';
			<?php endif; ?>
		</script>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<?php get_template_part('inc/icons'); ?>

		<?php wp_head(); ?>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<?php
			$now = date("U", current_time('timestamp'));
			$announce_settings = get_field('announce_time_settings', 'options');

			$afs_announce = '';
			$announce_status = false;

			if(strtotime($announce_settings['start_time']) <= $now && $now <= strtotime($announce_settings['end_time'])) {
				$announce_status = true;
				$afs_announce = 'afs-announce-on';
			}

		?>


<script type="text/javascript"> var _ss = _ss || []; _ss.push(['_setDomain', 'https://koi-3QNJYJ8YT6.marketingautomation.services/net']); _ss.push(['_setAccount', 'KOI-45AHXHF5U0']); _ss.push(['_trackPageView']); (function() { var ss = document.createElement('script'); ss.type = 'text/javascript'; ss.async = true; ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QNJYJ8YT6.marketingautomation.services/client/ss.js?ver=2.2.1'; var scr = document.getElementsByTagName('script')[0]; scr.parentNode.insertBefore(ss, scr); })(); </script>
		
	</head>

	<body <?php body_class($afs_announce); ?> role="document">


		<?php

			if ($announce_status): ?>
				<!-- Announcement from AFS -->
				<div class="afs-section afs-announce">

					<div class="afs-announce-label">
						<?php the_field("announce_label", "options"); ?>
					</div>

					<div class="afs-announce-message">
						<?php echo get_field("announce_message", "options"); ?>
					</div>

				</div>

		<?php
			endif;
		?>


		<header role="banner" id="header" class="navbar navbar-static-top">
			<div class="container">
				<?php get_template_part('nav'); ?>
			</div>
		</header>

		<?php get_sidebar('side-drawer'); ?>
