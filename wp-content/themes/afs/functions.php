<?php

include('inc/comments.php');
include('inc/excerpt_read_more.php');
include('inc/gravity_forms.php');
include('inc/misc_functions.php');
include('inc/navs.php');
include('inc/pagination.php');
include('inc/scripts.php');
include('inc/sidebars.php');
include('inc/site_options.php');
include('inc/social_links.php');
include('inc/thumbs.php');
include('inc/walker_nav.php');
include('inc/wp-admin.php');

include('inc/widget.php');
include('inc/hooks.php');

include('inc/woocommerce.php');
