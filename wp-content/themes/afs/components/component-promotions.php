<?php
	$promotion_cat = get_field('inspiration_cat_product');

	$now = date("Y-m-d H:i:s", current_time('timestamp'));

	$args = array(
		'post_type' => 'promotion',
		'posts_per_page' => 4,
		'order' => 'DESC',
		'orderby' => 'date',
		'tax_query' => array(
			array(
				'taxonomy' => 'product_type',
				'field' => 'term_id',
				'terms' => $promotion_cat
			)
		),
		'meta_query' => array(
			'relation'	=> 'AND',
	        array(
	            'key' => 'promo_start',
	            'value' => $now,
	            'compare' => '<=',
	            'type' => 'DATE'
	        ),
	        array(
	            'key' => 'promo_end',
	            'value' => $now,
	            'compare' => '>=',
	            'type' => 'DATE'
	        )
	    )
	);

	$promotions = new WP_Query($args);
	// print_r($promotions);

?>


<?php if ($promotions->have_posts()): ?>
	<!-- Section: Promotions -->
	<section class="afs-section component-promotions">
		<div class="container">
			<div class="row">
				<h2 class="afs-title-lg">Current Promotions</h2>
				<?php
					while($promotions->have_posts()) :
						$promotions->the_post();
						get_template_part( 'components/element', 'promotion-card' );
					endwhile;

					wp_reset_postdata()
				?>
			</div>
		</div>
	</section>

<?php endif ?>
