<?php
	$watermark = get_field('placeholder_watermark', 'options');

	//Info
	$location_address = format_address( get_the_ID() );
	$location_gmap_link = get_field("location_gmap_link");
	$location_phone = get_field("location_phone");
	$location_additional_phone = get_field("location_additional_phone");

	$location_map_embed = get_field("location_map_embed");

	//Reviews
	$location_reviews = get_field("location_review_urls");

	//Hours
	$location_hours = get_field("location_hours");

?>


<!-- Header: Location -->
<section class="afs-section afs-location" style="background-image: url(<?php echo $watermark['url']; ?>)">

	<div class="container">
		<div class="row">
			<div class="location location-single col-lg-8 col-md-10">
				<h1 class="afs-title-lg"><?php the_title(); ?></h1>
				<div class="location-info">

					<div class="location-contact ">
						<?php if ($location_address && $location_gmap_link): ?>
							<address class="location-address">
								<a href="<?php echo $location_gmap_link; ?>" target="_blank">
									<?php echo $location_address; ?>
								</a>
							</address>
						<?php endif ?>

						<?php if ($location_phone): ?>
							<a class="location-phone" href="tel:+1<?php echo $location_phone; ?>"><?php echo format_phone( $location_phone ); ?></a>
						<?php endif ?>

						<?php if ($location_additional_phone): ?>

							<ul class="location-addphone">
								<?php foreach ($location_additional_phone as $key => $phone): ?>
									<li>
										<h6 class="location-addphone-label"><?php echo $phone['phone_label']; ?></h6>
										<a class="location-phone" href="tel:+1<?php echo $phone['phone_number']; ?>"><?php echo format_phone( $phone['phone_number'] ); ?></a>
									</li>
								<?php endforeach ?>
							</ul>

						<?php endif ?>

					</div>

					<?php if (!empty($location_reviews)): ?>
						<div class="location-review">
							<?php foreach ($location_reviews as $key => $network):

								if(!$network['item_show']) {
									continue;
								}

							?>
								<a class="review-<?php echo $network['item_network']['value']; ?>" href="<?php echo $network['item_url']; ?>" target="_blank"><?php echo $network['item_network']['label']; ?> Reviews</a>
							<?php endforeach ?>
						</div>
					<?php endif ?>
				</div>

				<?php if ($location_hours): ?>
					<div class="location-hours">

						<?php foreach ($location_hours as $key => $set):
							if (!empty($set['hours'])): ?>
								<div class="hours<?php echo (!empty($set['info']['footnote'])) ? ' hours-footer' : ''; ?>">
									<h3><?php echo $set['info']['label']; ?></h3>

									<?php foreach ($set['hours'] as $key => $day): ?>
										<dl>
											<dt><?php echo $day['day'] ?></dt>
											<dd><?php echo $day['time'] ?></dd>
										</dl>
									<?php endforeach;

									if(!empty($set['info']['footnote'])) : ?>

										<span class="footnote"><?php echo $set['info']['footnote'] ?></span>

								<?php
									endif;
								?>

								</div>
						<?php
							endif;
						endforeach; ?>
					</div>
				<?php endif ?>
			</div>
		</div>
	</div>
</section>