<?php
	$backgrounds['afs-sp-wholesale-2018'] = get_sub_field('background_image');
	$image = get_sub_field('foreground_image');
?>

<!-- Section: Enter to Win -->
<section class="afs-section afs-sp-wholesale-2018">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-lg-offset-6 col-md-8 col-md-offset-2 content">
				<img src="<?php echo $image['url'] ?> " alt="<?php echo $image['alt'] ?>">

				<div class="content__info">

					<div class="content__location">
						<h3>Location</h3>

						<p>
							America's Floor Source<br/>
							Columbus East
						</p>

						<a href="https://www.google.com/maps/place/America's+Floor+Source/@40.0186666,-82.9208148,17z/data=!4m13!1m7!3m6!1s0x88388a1b402a12a5:0x757ecb51abbbb62b!2s3442+Millennium+Ct,+Columbus,+OH+43219!3b1!8m2!3d40.0186666!4d-82.9186261!3m4!1s0x88388a1b402a12a5:0xc682c6023321a418!8m2!3d40.018491!4d-82.918614" target="_blank" rel="noopener">
							<address>
								3442 Millennium Court,<br/>
								Columbus, Ohio 43219<br/>
								Citygate Business Park
							</address>
						</a>

						<a class="btn--square" href="https://www.google.com/maps/place/America's+Floor+Source/@40.0186666,-82.9208148,17z/data=!4m13!1m7!3m6!1s0x88388a1b402a12a5:0x757ecb51abbbb62b!2s3442+Millennium+Ct,+Columbus,+OH+43219!3b1!8m2!3d40.0186666!4d-82.9186261!3m4!1s0x88388a1b402a12a5:0xc682c6023321a418!8m2!3d40.018491!4d-82.918614" target="_blank" rel="noopener">Get Directions</a>

					</div>

					<div class="content__hours">
						<h3>Hours</h3>

						<p>
							Friday, January 5th<br/>
							11:00am - 8:00pm
						</p>

						<p>
							Saturday, January 6th<br/>
							9:00am - 8:00pm
						</p>

						<p>
							Sunday, January 7th<br/>
							10:00am - 6:00pm
						</p>

					</div>


				</div>
			</div>
		</div>
	</div>

	<?php echo responsive_backgrounds_full ($backgrounds) ?>
</section>