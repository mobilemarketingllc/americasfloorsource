
	<!-- Modals -->
	<!-- Modal: Locations -->

	<?php

		//Images
		$modal_bg_loc = get_field("home_modal_loc_image");
		$modal_bg_apt = get_field("home_modal_apt_image");


		$locations_array = get_all_locations();

		//Separate Locations by state
		$locations_filtered = array();
		foreach ($locations_array as $key => $loc) {
			$state = get_field("location_state", $loc->ID);
			$locations_filtered[$state['label']][] = $loc;
		}

	?>



	<div id="modal-location" class="modal-location modal fade" tabindex ="-1" role="dialog" aria-labelledby="Locations">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<button class="modal-close" type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fa fa-times-circle" aria-hidden="true"></i>
				</button>
				<div class="modal-image">
					<img src="<?php echo $modal_bg_loc['url'] ?>" alt="<?php echo $modal_bg_loc['alt'] ?>">
				</div>
				<div class="modal-body">
					<h2 class="afs-title-skinny">Find a Showroom Near You</h2>

					<?php foreach ($locations_filtered as $key => $locations): ?>
						<div class="row">
							<h4 class="modal-state-title col-lg-12"><?php echo $key ?></h4>
							<?php foreach ($locations as $key => $loc):
								$phone = get_field("location_phone", $loc->ID);
							?>

								<div class="location col-lg-4 col-xs-6">
									<h4 class="location-title"><?php echo the_field("location_shortname", $loc->ID); ?></h4>
									<address>
										<a class="info-address" href="<?php echo get_field("location_gmap_link", $loc->ID ); ?>">
											<?php echo format_address( $loc->ID ); ?>
										</a>
									</address>

									<?php if ($phone): ?>
										<a href="tel:+1<?php echo $phone ?>"><?php echo format_phone($phone); ?></a>
									<?php endif ?>

								</div>

							<?php endforeach ?>
						</div>

					<?php endforeach ?>

				</div>
			</div>
		</div>
	</div>

	<!-- Modal: Appointment -->

	<div id="modal-appointment" class="modal-appointment modal fade" tabindex="-1" role="dialog" aria-labelledby="Schedule an Appointment">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<button class="modal-close" type="button" class="close" data-dismiss="modal" aria-label="Close">
					<i class="fa fa-times-circle" aria-hidden="true"></i>
				</button>
				<div class="modal-image">
					<img src="<?php echo $modal_bg_apt['url']; ?>" alt="<?php echo $modal_bg_apt['alt']; ?>">
				</div>

				<div class="modal-body">
					<?php the_field('home_modal_apt_form'); ?>
				</div>

			</div>
		</div>
	</div>


	<!-- End Modals -->
