<?php
	//Component: Half Paged Content


	$style = get_field("hpc_style");
	$format = (get_field("hpc_format")) ? get_field("hpc_format") : 'normal';

	$component_bg['component-hpc'] = get_field("hpc_bg")
?>


<section class="afs-section component-hpc component-hpc-<?php echo $style ?>">
	<?php
		//Print out Responsive Background CSS
		echo responsive_backgrounds_full($component_bg); ?>
	<div class="container">
		<div class="row">
			<div class="hpc-content afs-CAS-1 hpc-content<?php echo ($format == 'col-list') ? '-col' : '' ; ?> col-xl-6 col-lg-7 col-md-7 col-sm-7">
				<?php

					if($format == 'normal') :

						the_field("hpc_content");

					elseif($format == 'col-list'):
						$col_content = get_field("hpc_content_col");
				?>

						<h2 class="afs-title-lg"><?php echo get_field("hpc_content_col_title") ?></h2>

						<?php foreach ($col_content as $key => $col): ?>
							<div class="content-col col-lg-3">
								<h4 class="afs-title-sm"><?php echo $col['col_title'] ?></h4>
								<p><?php echo $col['col_body'] ?></p>
							</div>
						<?php endforeach;

					endif;

				?>

			</div>
		</div>
	</div>

</section>