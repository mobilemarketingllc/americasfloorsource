<?php
	// Template Name: B2B

	get_header();

	$formID = get_field("b2b_form_id");
?>

<main id="main" role="main">

	<!-- Section: Header Large -->
	<?php include('components/header-skinny.php'); ?>

	<!-- Section: Content with Sidebar -->
	<section class="afs-section afs-b2b">
		<div class="container">
			<div class="row">
				<div class="afs-b2b-main afs-CAS-1 col-lg-9">
					<?php the_field("b2b_content"); ?>
				</div>

				<div class="afs-b2b-sidebar col-lg-3">
					<?php get_sidebar('b2b'); ?>
				</div>
			</div>
		</div>
	</section>

	<!-- Section: Testimonial -->
	<?php include('components/component-testimonial.php'); ?>

	<?php if ($formID): ?>
		<!-- Section: B2B Form -->
		<section id="afs-b2b-form" class="afs-section afs-b2b-form">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<?php

						gravity_form( $formID, true, false, false, null, false, 0, true ); ?>
					</div>
				</div>
			</div>
		</section>
	<?php endif ?>


</main>

<?php

	get_footer();

?>
