<?php
	get_header();

	$tt_categories = wp_get_post_terms(
		get_the_ID(),
		'tt_cat',
		array(
			'orderby' => 'taxonomy',
			'order' => 'ASC'
		)
	);


	$content = get_field("tat_content");
	$post_header = get_field("header_bg");

?>

<main id="main" role="main">

	<!-- Header: Large -->
	<section class="afs-post-header">
		<picture>
			<source media="(min-width: 1440px)" srcset="<?php echo $post_header['sizes']['hero-full']; ?>">
			<source media="(min-width: 1200px)" srcset="<?php echo $post_header['sizes']['hero-lg']; ?>">
			<source media="(min-width: 992px)" srcset="<?php echo $post_header['sizes']['hero-md']; ?>">
			<source media="(min-width: 768px)" srcset="<?php echo $post_header['sizes']['hero-sm']; ?>">
			<source media="(min-width: 1px)" srcset="<?php echo $post_header['sizes']['hero-xs']; ?>">
			<img src="<?php echo $post_header['url']; ?>" alt="<?php echo $post_header['alt']; ?>">
		</picture>
	</section>

	<!-- Section: Tips & Trends with Sidebar -->
	<section class="afs-section afs-tat">
		<div class="container">
			<div class="row">
				<div class="tat-content col-lg-9">
					<div class="tat-header">
						<h1 class="afs-title-lg"><?php the_title(); ?></h1>
						<ul class="tat-meta">
							<li><?php echo display_inline_cat($tt_categories, 'inline_category', get_bloginfo('url') . '/tips-and-trends/', 'type'); ?></li>
							<li>Posted <?php echo get_the_date('F j, Y'); ?></li>
						</ul>
					</div>
					<div class="tat-body">
						<?php foreach ($content as $key => $value): ?>

							<div class="<?php echo $value['acf_fc_layout']; ?>">
								<?php

									switch ($value['acf_fc_layout']) {
										case 'tat-image':
											echo '<img src="' . $value['tat_image_image']['url'] . '" alt="' . $value['tat_image_image']['alt'] . '">';
											break;

										case 'tat-text':
											echo $value['tat_text_text'];
											break;

										case 'tat-text-1col':
											echo $value['tat_text_text'];
											break;

										case 'tat-quote':
											echo $value['tat_quote_text'];
											break;

										case 'tat-icons':

											include('components/component-layout-tat-icons.php');

											break;

									}

								?>
							</div>

						<?php endforeach ?>
					</div>
				</div>
				<div class="tat-sidebar col-lg-3">
					<?php include('sidebar-tat.php'); ?>
				</div>
			</div>
		</div>
	</section>

	<!-- Section: Split Call to Action -->
	<?php include('components/component-splitcta.php'); ?>

	<!-- Section: Financing Section -->
	<?php include('components/component-inspirationcta.php'); ?>

</main>

<?php

	get_footer();

?>
