<?php
/**
 * Single product short description
 *
 *	Modifications
 *	- Remove default short_description
 *	- Add custom product meta flexible content
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  Automattic
 * @package WooCommerce/Templates
 * @version 3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $post;
$short_description = apply_filters( 'woocommerce_short_description', $post->post_excerpt );
?>


<div class="custom-description">
	<?php
		if ( $short_description ) {
			echo $short_description;
		}
	?>
</div>
