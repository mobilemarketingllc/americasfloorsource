<?php
/**
 * Edit account form
 *
 * Modifications:
 * - Wrap inputs in span.woocommerce-input-wrapper to make the form consistent with other forms
 * - Add placeholders to fields
 * - Add "Account Details" title
 * - Make the input tags more readable
 *
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_edit_account_form' ); ?>

<form class="woocommerce-EditAccountForm edit-account" action="" method="post">

	<?php do_action( 'woocommerce_edit_account_form_start' ); ?>


	<h1 class="dashboard-title">Account details</h1>

	<p class="woocommerce-form-row woocommerce-form-row--first form-row form-row-first">
		<label for="account_first_name"><?php esc_html_e( 'First name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<span class="woocommerce-input-wrapper">
			<input
				type="text"
				class="woocommerce-Input woocommerce-Input--text input-text"
				name="account_first_name"
				id="account_first_name"
				autocomplete="given-name"
				placeholder="First Name"
				value="<?php echo esc_attr( $user->first_name ); ?>" />
		</span>
	</p>
	<p class="woocommerce-form-row woocommerce-form-row--last form-row form-row-last">
		<label for="account_last_name"><?php esc_html_e( 'Last name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<span class="woocommerce-input-wrapper">
			<input
				type="text"
				class="woocommerce-Input woocommerce-Input--text input-text"
				name="account_last_name"
				id="account_last_name"
				autocomplete="family-name"
				placeholder="Last Name"
				value="<?php echo esc_attr( $user->last_name ); ?>" />
		</span>
	</p>
	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_display_name"><?php esc_html_e( 'Display name', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<span class="woocommerce-input-wrapper">
			<input
				type="text"
				class="woocommerce-Input woocommerce-Input--text input-text"
				name="account_display_name"
				id="account_display_name"
				placeholder="Display Name"
				value="<?php echo esc_attr( $user->display_name ); ?>" />
		</span>
		<span><em><?php esc_html_e( 'This will be how your name will be displayed in the account section and in reviews', 'woocommerce' ); ?></em></span>
	</p>
	<div class="clear"></div>

	<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
		<label for="account_email"><?php esc_html_e( 'Email address', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<span class="woocommerce-input-wrapper">
			<input
				type="email"
				class="woocommerce-Input woocommerce-Input--email input-text"
				name="account_email"
				id="account_email"
				autocomplete="email"
				placeholder="Email"
				value="<?php echo esc_attr( $user->user_email ); ?>" />
		</span>
	</p>

	<fieldset>
		<legend><?php esc_html_e( 'Password change', 'woocommerce' ); ?></legend>

		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_current"><?php esc_html_e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<span class="woocommerce-input-wrapper">
				<input
					type="password"
					class="woocommerce-Input woocommerce-Input--password input-text"
					name="password_current"
					id="password_current"
					placeholder="<?php esc_html_e( 'Current password (leave blank to leave unchanged)', 'woocommerce' ); ?>"
					autocomplete="off" />
			</span>
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_1"><?php esc_html_e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?></label>
			<span class="woocommerce-input-wrapper">
				<input
					type="password"
					class="woocommerce-Input woocommerce-Input--password input-text"
					name="password_1"
					id="password_1"
					placeholder="<?php esc_html_e( 'New password (leave blank to leave unchanged)', 'woocommerce' ); ?>"
					autocomplete="off" />
			</span>
		</p>
		<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
			<label for="password_2"><?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?></label>
			<span class="woocommerce-input-wrapper">
				<input
					type="password"
					class="woocommerce-Input woocommerce-Input--password input-text"
					name="password_2"
					id="password_2"
					placeholder="<?php esc_html_e( 'Confirm new password', 'woocommerce' ); ?>"
					autocomplete="off" />
			</span>
		</p>
	</fieldset>
	<div class="clear"></div>

	<?php do_action( 'woocommerce_edit_account_form' ); ?>

	<p>
		<?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
		<button type="submit" class="woocommerce-Button button" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save changes', 'woocommerce' ); ?></button>
		<input type="hidden" name="action" value="save_account_details" />
	</p>

	<?php do_action( 'woocommerce_edit_account_form_end' ); ?>
</form>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>
