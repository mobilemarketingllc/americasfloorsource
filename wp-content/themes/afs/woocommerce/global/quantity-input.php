<?php
/**
 * Product quantity inputs
 *
 * 	Modifiactions
 * 	- Move label after input field
 * 	- Add logic to change Label from Quantity to Sqft
 * 	- Remove Screen reader only class from label
 *  - Add class 'afs-auto-validate' so that we can auto validate the quantity fields with Javascript when necessary
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;


$label = esc_html( 'Quantity', 'woocommerce' );

$extra_class = '';

if(isRolled($product)) {
	$label = esc_html( 'SqFt', 'woocommerce' );
	$extra_class = ' afs-auto-validate';
}

if(isCarton($product)) {
	$extra_class = ' afs-auto-sqft';
}

if ( $max_value && $min_value === $max_value ) {
	?>
	<div class="quantity hidden">
		<input type="hidden" id="<?php echo esc_attr( $input_id ); ?>" class="qty" name="<?php echo esc_attr( $input_name ); ?>" value="<?php echo esc_attr( $min_value ); ?>" />
	</div>
	<?php
} else {
	/* translators: %s: Quantity. */
	$labelledby = ! empty( $args['product_name'] ) ? sprintf( __( '%s quantity', 'woocommerce' ), strip_tags( $args['product_name'] ) ) : '';
	?>
	<div class="quantity">
		<input
			type="number"
			id="<?php echo esc_attr( $input_id ); ?>"
			class="input-text qty text<?php echo $extra_class; ?> "
			step="<?php echo esc_attr( $step ); ?>"
			min="<?php echo esc_attr( $min_value ); ?>"
			max="<?php echo esc_attr( 0 < $max_value ? $max_value : '' ); ?>"
			name="<?php echo esc_attr( $input_name ); ?>"
			value="<?php echo esc_attr( $input_value ); ?>"
			title="<?php echo esc_attr_x( 'Qty', 'Product quantity input tooltip', 'woocommerce' ); ?>"
			size="4"
			pattern="<?php echo esc_attr( $pattern ); ?>"
			inputmode="<?php echo esc_attr( $inputmode ); ?>"

			<?php if(isCarton($product)) : ?>
				data-sqftpc="<?php the_field('sqft_per_carton', $product->get_ID()); ?>"
			<?php endif; ?>

			aria-labelledby="<?php echo esc_attr( $labelledby ); ?>" />
		<label for="<?php echo esc_attr( $input_id ); ?>"><?php echo $label; ?></label>
	</div>
	<?php
}
