<?php if ( is_active_sidebar( 'sidebar-default' ) ) : ?>
	<ul id="sidebar-widgets">
		<?php dynamic_sidebar( 'sidebar-default' ); ?>
	</ul>
<?php endif; ?>
