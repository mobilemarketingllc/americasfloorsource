<?php
	//Page: Home
	get_header();

	$now = date("U", current_time('timestamp'));

	$promotion_start = strtotime(get_field('header_start'));
	$promotion_end = strtotime(get_field('header_end'));

	if($promotion_start <= $now && $now <= $promotion_end) {
		$header_bg_d = get_field("header_bg_promotion_d");
		$header_bg_m = get_field("header_bg_promotion_m");
		$style = 'header-home-promotion';
		$class = get_field("header_special_class");
	} else {

		$header_bg['header-home'] = get_field("header_bg_normal");
		$style = 'header-home-modal';
		$class = '';
	}

	$ecomEnabled = get_field('enable_ecommerce', 'options');
?>
<main id="main" role="main">
	<!-- Section: Header Home -->
	<section class="afs-section header-home <?php echo $style . ' ' . $class ?>">
		<?php
			if ($style == 'header-home-modal') {
				include('components/element-fp-modals.php');
			}

			//Promotion Style
			if ($style == 'header-home-promotion'):
		?>
				<picture>
					<source media="(min-width: 1200px)" srcset="<?php echo $header_bg_d['sizes']['hero-full']; ?>">
					<source media="(min-width: 768px)" srcset="<?php echo $header_bg_d['sizes']['promo-lg']; ?>">
					<source media="(max-width: 767px)" srcset="<?php echo $header_bg_m['url']; ?>">
					<img src="<?php echo $header_bg_d['url']; ?>" alt="<?php echo $header_bg_d['alt']; ?>">
				</picture>

				<?php $header_pagelinks = get_field("header_link"); ?>

				<?php if ($header_pagelinks): ?>

					<div class="header-content">
						<div class="header-btns">

							<?php foreach ($header_pagelinks as $key => $btn): ?>

								<a class="btn--square" href="<?php echo $btn['link_url']; ?>"><?php echo $btn['link_label']; ?></a>

							<?php endforeach ?>

						</div>
					</div>

				<?php endif ?>

		<?php

			//Normal Style
			else :
				//Print out Responsive Background CSS
				echo responsive_backgrounds_full($header_bg);
		?>
				<div class="header-content">
					<h1 class="header-title"><?php the_field("header_title")?></h1>
					<div class="header-btns">
						<button class="btn--square" type="button" data-toggle="modal" data-target=".modal-location">Shop in Store</button>
						<button class="btn--square" type="button" data-toggle="modal" data-target=".modal-appointment">Shop at Home</button>
						<?php if($ecomEnabled && $link = get_field('hero_link')): ?>
							<a class="btn--square" href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>
						<?php endif; ?>
					</div>
				</div>

		<?php endif; ?>

	</section>

	<!-- Section: Product Bar -->
	<?php include('components/component-productbar.php'); ?>

	<!-- Section: Half Page content -->
	<?php include('components/component-hpc.php'); ?>

	<!-- Section: Split Call to Action -->
	<?php include('components/component-splitcta.php'); ?>

	<!-- Section: Testimonial -->
	<?php include('components/component-testimonial.php'); ?>

	<!-- Section: Financing Section -->
	<?php include('components/component-financingcta.php'); ?>

	<!-- Section: Inspirations Section -->
	<?php include('components/component-inspirationcta.php'); ?>

</main>

<?php

	get_footer();

?>
