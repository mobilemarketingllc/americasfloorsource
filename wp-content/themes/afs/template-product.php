<?php
	//Template Name: Product
	get_header();

	$product_category = $post->post_name;

	$tabbed_content_raw = get_field("hpct_tabs");

?>

<main id="main" role="main">

	<!-- Section: Header Skinny -->
	<?php include('components/header-skinny.php'); ?>

	<!-- Section: Content + Acordian -->
	<section class="afs-section afs-hpct">
		<div class="container">
			<div class="row">
				<div class="hpct-content col-lg-6">
					<?php echo get_field('hpct_content'); ?>
				</div>

				<?php
					if($tabbed_content_raw) :

						$tabbed_content_sorted = [];

						foreach ($tabbed_content_raw as $key => $item) :

							if(!isset($tabbed_content_sorted[$item['tab_tab']['value']]['labels'])) {
								$tabbed_content_sorted[$item['tab_tab']['value']]['label'] = $item['tab_tab']['label'];
							}

							$tabbed_content_sorted[$item['tab_tab']['value']]['content'][] = $item;

						endforeach;


				?>

						<div class="hpct-tabs afs-tabs col-lg-6">

							<ul class="tabs">
							  	<?php
							  		$i = 0;
							  		foreach ($tabbed_content_sorted as $key => $tab) :

							  			$class = ($i == 0) ? ' class="active"' : '';
							  	?>

									<li <?php echo $class; ?> rel="<?php echo 'tab' . $i ?>"><?php echo $tab['label']; ?></li>

								<?php
										$i++;
									endforeach;
								?>
							</ul>

							<div class="tab-container">

								<?php
									$i = 0;
									foreach ($tabbed_content_sorted as $key => $tab):

							  			$class = ($i == 0) ? ' d-active' : '';
							  			$style = ($i == 0) ? ' style="display: block;"' : ' style="display: none;"';
								?>
									<div class="tab-drawer-heading<?php echo $class; ?>" rel="<?php echo 'tab' . $i ?>"><h5><?php echo $tab['label'] ?></h5></div>
								    <div id="<?php echo 'tab' . $i ?>" class="tab-content" <?php echo $style; ?>>
										<?php foreach ($tab['content'] as $key => $item): ?>

											<div class="item">
												<img class="item-image" src="<?php echo $item['tab_image']['url'] ?>" alt="<?php echo $item['tab_image']['alt'] ?>">
												<h4 class="item-title"><?php echo $item['tab_title'] ?></h4>
												<p class="item-body"><?php echo $item['tab_body'] ?></p>
											</div>

										<?php endforeach ?>
								    </div>
								<?php
										$i++;
									endforeach;
								?>

							</div>

						</div>
					<?php endif;?>

  				</div>
			</div>
		</div>
	</section>

	<!-- Section: Financing Section -->
	<?php include('components/component-financingcta.php'); ?>

	<!-- Section: Half Page content -->
	<?php include('components/component-hpc.php'); ?>

	<!-- Section: Split Call to Action -->
	<?php include('components/component-splitcta.php'); ?>

	<!-- Section: Inspirations Section -->
	<?php include('components/component-inspirationcta.php'); ?>

	<!-- Section: Testimonial -->
	<?php include('components/component-testimonial.php'); ?>

	<!-- Section: Product Bar -->
	<?php include('components/component-productbar.php'); ?>

</main>

<?php

	get_footer();

?>
