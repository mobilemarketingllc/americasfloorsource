<?php
	remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
	add_action('woocommerce_before_main_content', 'afs_woocommerce_header', 10);
	add_action('woocommerce_before_cart', 'afs_woocommerce_header', 10);
	function afs_woocommerce_header() {
?>
		<div class="woocommerce-header">
			<?php woocommerce_breadcrumb(); ?>
			<nav class="woocommerce-header-nav">
				<ul>
					<li>
						<a class="woocommerce-header-nav__account icon-account" href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">Account</a>
					</li>
					<li>
						<a class="woocommerce-header-nav__cart icon-cart" href="<?php echo wc_get_cart_url() ?>">Cart<?php echo get_cart_count() ?></a>
					</li>
				</ul>
			</nav>
		</div>
<?php
	}


	/**
	 * Change the breadcrumb separator
	 */
	add_filter( 'woocommerce_breadcrumb_defaults', 'afs_change_breadcrumb_delimiter' );
	function afs_change_breadcrumb_delimiter( $defaults ) {
		// Change the breadcrumb delimeter from '/' to '>'
		$defaults['delimiter'] = ' &gt; ';
		$defaults['home'] =_x( 'Shop', 'breadcrumb', 'woocommerce' );
		return $defaults;
	}

	/**
	 * Replace the home link URL in WooCommerce Breadcrumb
	 */
	add_filter( 'woocommerce_breadcrumb_home_url', 'woo_custom_breadrumb_home_url' );
	function woo_custom_breadrumb_home_url() {
		return get_permalink( woocommerce_get_page_id( 'shop' ) );
	}

	/**
	 * Unhook Taxonomy and Product archive description from archive page
	 */
	remove_action('woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10);
	remove_action('woocommerce_archive_description', 'woocommerce_product_archive_description', 10);


	/**
	 * Remove the add to cart button on archive.
	*/
	remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);
	/**
	 * Add view product button to product loop
	 */
	add_action( 'woocommerce_after_shop_loop_item', 'archive_view_product_button' );
	function archive_view_product_button() {
		echo '<a class="button" href="' . get_the_permalink() . '">View Product</a>';
	}


	/**
	 * Change number or products per row to 3
	 */
	add_filter('loop_shop_columns', 'afs_loop_columns');
	function afs_loop_columns() {
		return 3;
	}


	/**
	 * Change the placeholder image
	 */
	add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');
	function custom_woocommerce_placeholder_img_src( $src ) {
		$placeholder = get_field('placeholder_image', 'options');
		return $placeholder['url'];
	}


	/**
	 * Add lightbox functionality to Single Product Page
	 */
	add_action( 'after_setup_theme', 'woo_product_gallery_supports' );
	function woo_product_gallery_supports() {
		add_theme_support( 'wc-product-gallery-lightbox' );
	}


	/**
	 * Remove Data Tabs and Upsell
	 */
	remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
	remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);

	/**
	 * Remove Cross Sells
	 */
	remove_action('woocommerce_cart_collaterals', 'woocommerce_cross_sell_display', 10);


	/* Displays all Product Attributes */
	add_action('woocommerce_single_product_summary', 'display_attributes', 25);
	function display_attributes(){
		global $product;
		$attributes = $product->get_attributes();
		if ( ! $attributes ) {
			return;
		}

		$display_result = '<ul class="product_attributesList">';

		$result_array = [];

		foreach ( $attributes as $attribute ) {


			if ( $attribute->get_variation() ) {
				continue;
			}

			$name = $attribute->get_name();
			if ( $attribute->is_taxonomy() ) {

				$terms = wp_get_post_terms( $product->get_id(), $name, 'all' );

				if(count($terms)) {
					$cwtax = $terms[0]->taxonomy;

					$cw_object_taxonomy = get_taxonomy($cwtax);

					if ( isset ($cw_object_taxonomy->labels->singular_name) ) {
						$tax_label = $cw_object_taxonomy->labels->singular_name;
					} elseif ( isset( $cw_object_taxonomy->label ) ) {
						$tax_label = $cw_object_taxonomy->label;
						if ( 0 === strpos( $tax_label, 'Product ' ) ) {
							$tax_label = substr( $tax_label, 8 );
						}
					}

					$tax_terms = array();
					foreach ( $terms as $term ) {
						$single_term = esc_html( $term->name );
						array_push( $tax_terms, $single_term );
					}

					$result_array[] = '<li><span class="label">' . $tax_label . '</span><span class="value">' . implode(', ', $tax_terms) . '</span></li>';
				}

			} else {
				$result_array[] = '<li><span class="label">' . $name . '</span><span class="value">' . esc_html( implode( ', ', $attribute->get_options() ) ) . '</span></li>';
			}
		}

		//Add sqft per carton if needed
		if(isCarton($product)) {
			$result_array[] = '<li><span class="label">SqFt per Carton</span><span class="value">' . get_field('sqft_per_carton', $product->get_ID()) . '</span></li>';
		}


		sort($result_array);

		$display_result .= implode('', $result_array);
		$display_result .= '</ul>';

		echo $display_result;
	}

	/* Add quanitity notice to product page.
	 *	- This notice indicates that their desired sqft has been updated to meet min/step requirements
	*/
	add_action('woocommerce_single_product_summary', 'display_carton_quantity_sqft', 35);
	function display_carton_quantity_sqft() {
		global $product;

		if(isCarton($product)) {
			$sqftpc = get_field('sqft_per_carton', $product->get_ID());

			if($sqftpc) {
				$html = '<div class="product-sqft-notice">
							<p>This order would cover <span class="quantity">' . $sqftpc . '</span> sqft.</p>
						</div>';

				echo $html;
			}
		}

	}

	/* Add quanitity notice to product page.
	 *	- This notice indicates that their desired sqft has been updated to meet min/step requirements
	*/
	add_action('woocommerce_single_product_summary', 'display_quantity_notice', 30);
	function display_quantity_notice() {
		global $product;

		if(isRolled($product)) {
			$step = get_field('product_width', $product->get_ID());

			if($step > 1) {
				$min = get_min_product_quantity($step);

				$html = '<div class="product-quantity-notice">
							<p>Note: This product must meet the minimum amount (' . $min . ' sqft.) ordered and be ordered in multiples of ' . $step . '. We have adjusted the quantity inserted in the quantity field to meet these requirements and to cover the amount of square footage requested.</p>
						</div>';

				echo $html;
			}
		}

	}

	/**
	 * Adjust the quantity input values for Rolled/Sheet products
	 */
	add_filter( 'woocommerce_quantity_input_args', 'adjust_woocommerce_quantity_input_args', 10, 2 ); // Simple products
	function adjust_woocommerce_quantity_input_args( $args, $product ) {

		if(isRolled($product)) {

			$step = get_field('product_width', $product->get_ID());

			if(!$step || !is_numeric($step) ) {
				return $args;
			}

			$min = get_min_product_quantity($step);

			if($args['input_value'] < $min) {
				$args['input_value'] = $min;	// Starting value (we only want to affect product pages, not cart)
			}

			$args['min_value'] 	= $min;   	// Minimum value
			$args['step'] 		= $step;    // Quantity steps
		}

		return $args;
	}

	/***
	 * Add linear ft to product meta data. Rolled products are sold in sqft but AFS needs to know how many linear ft to
	 * pull instead of total sqft
	*/
	add_action('woocommerce_checkout_order_processed', 'add_linear_ft_to_rolled', 10, 3);
	function add_linear_ft_to_rolled($order_id, $posted_data, $order) {

		$order_items = $order->get_items();

		foreach ($order_items as $key => $item) {

			$product_id = $item->get_product_id();
			$isRolled = isRolled($product_id);

			if($isRolled) {

				$product_width = get_field('product_width', $product_id);
				$sqft = $item->get_quantity();

				if($product_width && $sqft) {
					$linear_ft = $sqft/$product_width . 'ft';
					wc_add_order_item_meta( $item->get_ID(), 'Linear Ft', $linear_ft);
				}
			}
		}
	}

	/**
	 * Hide the Linear Feet Order Item meta data on the front end.
	 */
	add_filter( 'woocommerce_order_item_get_formatted_meta_data', 'hide_rolled_linear_ft', 10, 1 );
	function hide_rolled_linear_ft( $formatted_meta ) {
		if(!is_admin()) {
			$formatted_meta = array_filter($formatted_meta, function($item) {
				return $item->key !== 'Linear Ft';
			});
		}

		return $formatted_meta;
	}

	/**
	 * Add the /sqft to products
	 */
	add_filter( 'woocommerce_get_price_suffix', 'add_sqft_suffix', 99, 2 );
	function add_sqft_suffix( $html, $product ){

		if(get_field('show_price_as_sqft', $product->get_ID()) && get_field('price_per_sqft', $product->get_ID()) && !is_admin()) {
			$html .= '<span class="price-suffix"> / sqft</span>';
		}

		return $html;
	}

	/**
	 * Show the price in $/sqft for carton products
	 */
	add_filter( 'woocommerce_get_price_html', 'display_price_per_sqft', 10, 2 );
	function display_price_per_sqft( $price, $product ) {

		if(isCarton($product) && get_field('show_price_as_sqft', $product->get_ID()) && !is_admin()) {

			$oldPrice = $product->get_price();
			$pricePerSqft = get_field('price_per_sqft', $product->get_ID());

			if(!empty($pricePerSqft)) {
				$price = str_replace($oldPrice, $pricePerSqft, $price );
			}

		}

		return $price;
	}

	/**
	 * Add Total sqft under product name in cart for carton products
	 */
	add_action( 'woocommerce_after_cart_item_name', 'add_sqft_to_carton_product_in_cart', 1 );
	function add_sqft_to_carton_product_in_cart($cart_item) {

		if(isCarton($cart_item['product_id'])) {
			$pricePerSqft = get_field('sqft_per_carton', $cart_item['product_id']);
			$sqft = round($pricePerSqft * $cart_item['quantity'], 2);

			echo '<p class="sqft-ft">' . $sqft . ' sqft</p>';
		}
	}


	/**
	 * Add disclaimer to stock information
	*/
	add_filter( 'woocommerce_get_stock_html', 'add_to_stock_html', 20, 2 );
	function add_to_stock_html( $html ) {

		if(!empty($html)) {
			$html .= '<p>Availability subject to change. Customer will be contacted in the event of a change to a product’s availability.</p>';
		}

		return $html;
	}