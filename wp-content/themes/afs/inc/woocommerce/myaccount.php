<?php

	/**
	 * Removes the 'Download' link in the 'My Account' menu
	 */
	add_filter ( 'woocommerce_account_menu_items', 'remove_downloads_link_in_myaccount_menu' );
	function remove_downloads_link_in_myaccount_menu( $menu_links ){
		unset( $menu_links['downloads'] ); // Downloads

		return $menu_links;
	}
