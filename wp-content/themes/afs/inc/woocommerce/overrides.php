<?php

	if ( ! function_exists( 'woocommerce_quantity_input' ) ) {

		/**
		 *  NOTE: Override the default woocommerce_quantity_input function so that we can pass the $product variable to the
		 *  quantity-input.php template. We need the $product to determine what label to use for the quantity (SqFt vs Quanitity)
		 */
		function woocommerce_quantity_input( $args = array(), $product = null, $echo = true ) {
			if ( is_null( $product ) ) {
				$product = $GLOBALS['product'];
			}

			$defaults = array(
				'input_id'     => uniqid( 'quantity_' ),
				'input_name'   => 'quantity',
				'input_value'  => '1',
				'max_value'    => apply_filters( 'woocommerce_quantity_input_max', -1, $product ),
				'min_value'    => apply_filters( 'woocommerce_quantity_input_min', 0, $product ),
				'step'         => apply_filters( 'woocommerce_quantity_input_step', 1, $product ),
				'pattern'      => apply_filters( 'woocommerce_quantity_input_pattern', has_filter( 'woocommerce_stock_amount', 'intval' ) ? '[0-9]*' : '' ),
				'inputmode'    => apply_filters( 'woocommerce_quantity_input_inputmode', has_filter( 'woocommerce_stock_amount', 'intval' ) ? 'numeric' : '' ),
				'product_name' => $product ? $product->get_title() : '',
			);

			$args = apply_filters( 'woocommerce_quantity_input_args', wp_parse_args( $args, $defaults ), $product );

			// Apply sanity to min/max args - min cannot be lower than 0.
			$args['min_value'] = max( $args['min_value'], 0 );
			$args['max_value'] = 0 < $args['max_value'] ? $args['max_value'] : '';

			// Max cannot be lower than min if defined.
			if ( '' !== $args['max_value'] && $args['max_value'] < $args['min_value'] ) {
				$args['max_value'] = $args['min_value'];
			}

			$args['product'] = $product;

			ob_start();

			wc_get_template( 'global/quantity-input.php', $args );

			if ( $echo ) {
				echo ob_get_clean(); // WPCS: XSS ok.
			} else {
				return ob_get_clean();
			}
		}
	}