<?php

	function get_min_product_quantity($step) {
		return ceil(100/$step) * $step;
	}

	function isCarton($product) {
		if(!$product) {
			return false;
		}

		if(is_numeric($product)) {
			$product = new WC_Product($product);
		}

		$sold_as = $product->get_attribute( 'pa_sold-as' );

		return strpos($sold_as, 'Carton') !== false;
	}

	function isRolled($product) {
		if(!$product) {
			return false;
		}

		if(is_numeric($product)) {
			$product = new WC_Product($product);
		}

		$sold_as = $product->get_attribute( 'pa_sold-as' );

		return strpos($sold_as, 'Rolled') !== false || strpos($sold_as, 'Sheet') !== false;
	}

	/**
	 * Get the number of unique items in the cart to display by the cart button
	 * - We do this since some products are sold by sqft which would make the number fairly large in some cases
	 *
	 * @return  string  returns the cart cound or an empty string
	 */
	function get_cart_count() {
		global $woocommerce;
		$cart_items = $woocommerce->cart->get_cart();
		return (count($cart_items)) ? '<span class="cart-count">' . count($cart_items) . '</span>' : '';
	}


	/**
	 * Indicate whether filters are active on the product archive pages
	 * @return  [string]
	 */
	function hasFilters() {
		$url = parse_url($_SERVER['REQUEST_URI']);

		$isCategory = is_numeric(strpos($url['path'], '/product_cat/'));
		$hasFilters = count(array_filter(explode('&', $url['query']), function($item) {
			return $item !== "";
		}));

		return ($isCategory || $hasFilters) ? true : false;
	}