<?php
/*
Register Sidebars
*/
register_sidebar(
	array(
		'name' => __('Tips & Trends Sidebar', 'bldwpjs'),
		'id' => 'sidebar-tat',
		'before_widget' => '<section class="widget %1$s %2$s"><div class="widget-inner">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="heading">',
		'after_title' => '</h3>',
	)
);

register_sidebar(
	array(
		'name' => __('B2B', 'bldwpjs'),
		'id' => 'sidebar-b2b',
		'before_widget' => '<section class="widget %1$s %2$s"><div class="widget-inner">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="heading">',
		'after_title' => '</h3>',
	)
);

register_sidebar(
	array(
		'name' => __('About', 'bldwpjs'),
		'id' => 'sidebar-about',
		'before_widget' => '<section class="widget %1$s %2$s"><div class="widget-inner">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="heading">',
		'after_title' => '</h3>',
	)
);

register_sidebar(
	array(
		'name' => __('Shop', 'bldwpjs'),
		'id' => 'sidebar-shop',
		'before_widget' => '<section class="widget %1$s %2$s"><div class="widget-inner">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="heading">',
		'after_title' => '</h3>',
	)
);
