<?php

//Sort Locations by menu_order
add_action( 'pre_get_posts', 'sort_locations');
function sort_locations($query){
		if(is_post_type_archive( 'location' )):
		 //If you wanted it for the archive of a custom post type use: is_post_type_archive( $post_type )
			 //Set the order ASC or DESC
			 $query->set( 'order', 'ASC' );
			 //Set the orderby
			 $query->set( 'orderby', 'menu_order' );
		endif;
};

//Ensure Formstack loads on ACF Site Options Page
add_action( 'admin_enqueue_scripts', 'formstack_site_options', 10, 1);
function formstack_site_options ($hook_suffix) {
	global $formstack;

	if ( 'toplevel_page_acf-options-site-options' === $hook_suffix ) {
		$forms = $formstack->get_forms();
		wp_localize_script( 'formstack-admin', 'formstack_forms', $forms );
		wp_localize_script( 'formstack-admin', 'formstack_tinymce', array(
			'button'        => esc_html( 'Formstack', 'formstack' ),
			'list_label'    => esc_html( 'Choose a form to embed:', 'formstack' ),
			'tinymce_title' => esc_html( 'Embed a Formstack form', 'formstack' ),
		) );
	}
}

//
/**
 * Manipulate the main navigation
 * - Add Sidebar drawer link to main nav for mobile
 * - Add the "cart count" to the My Cart link
 */
add_filter( 'wp_nav_menu_items', 'add_sidebar_link', 10, 2 );
function add_sidebar_link( $items, $args ) {
	$ecomEnabled = get_field('enable_ecommerce', 'options');

	if($args->menu->slug == 'main-navigation') {
		$items = '<li class="sidebar-button"><a href="#">' . get_field("sidebar-tab", "option") . '</a></li>' . $items;

		if(	$ecomEnabled ) {
			$items = str_replace(
				'<a href="' . get_bloginfo('siteurl') . '/cart/">Cart</a>',
				'<a href="' . get_bloginfo('siteurl') . '/cart/">Cart' . get_cart_count() . '</a>',
				$items
			);

		}

	}

	return $items;
}

//Hides the Shop links in the main menu when ecom is disabled
add_filter( 'wp_get_nav_menu_items', 'hide_shop_button_when_ecom_disabled', null, 3 );
function hide_shop_button_when_ecom_disabled( $items, $menu, $args ) {
	$ecomEnabled = get_field('enable_ecommerce', 'options');

	if(is_admin() || $ecomEnabled) {
		return $items;
	}

	// Iterate over the items to search and destroy
	foreach ( $items as $key => $item ) {

		switch ($item->object_id) {
			case '2724': //"Shop Online"
			case '2727': //Account
			case '2725': //Cart
				unset( $items[$key] );
			break;
		}
	}

	return $items;
}

/**
 * Redirect shop traffic to the home page when ecom is disabled
 * - Only redirect non-admins
 */
add_action( 'template_redirect', 'prevent_shop_access_when_ecom_disabled' );
function prevent_shop_access_when_ecom_disabled() {
	if((is_woocommerce() || is_cart( )|| is_checkout())
		&& !get_field('enable_ecommerce', 'options')
		&& !current_user_can('administrator')) {
			wp_redirect( home_url( '/' ) );
		die;
	}
}

/**
 * Prevent WP from deleting trashed posts automatically.
 * This was requested by the client.
 */
function wpb_remove_schedule_delete() {
	remove_action( 'wp_scheduled_delete', 'wp_scheduled_delete' );
}
add_action( 'init', 'wpb_remove_schedule_delete' );


/**
 * Dynamically populate checkbox field for filterMap with Product Attributes
 */
function acf_load_color_field_choices( $field ) {

	//Get all product attributes
	$array = wc_get_attribute_taxonomies();

	//Add attributes to the choices array
	foreach ($array as $key => $attr) {
		$field['choices']['pa_' . $attr->attribute_name] = $attr->attribute_label;
	}

	// return the field
	return $field;
}
add_filter('acf/load_field/name=product_category_filters', 'acf_load_color_field_choices');